# Repast Simphony - Circular Bus Tour

## About

Project developed as semestral project in [Advanced techniques of modeling and simulation](http://ects.upce.cz/predmet/KST/INTMS?lang=en&rocnik=2) at [University of Pardubice](http://www.upce.cz/en/index.html).

![Animation example](https://zdenekdrahos@bitbucket.org/zdenekdrahos/repast-simphony-circular-bus-tour/raw/tip/screenshots/animation-example.png "Animation example")

## Used framework

* [Repast Simphony 2.1](http://repast.sourceforge.net/) - Java-based modeling system

## Facts

License: [WTFPL](http://www.wtfpl.net/).

Author: [Zdenek Drahos](https://bitbucket.org/zdenekdrahos).
