package circularBusTour.entities.factory;

import circularBusTour.entities.BusStop;
import circularBusTour.entities.BusStopFactory;

public class BasicBusStopFactory implements BusStopFactory {

    @Override
    public BusStop create(int id) {
        return new BusStop(id);
    }
}
