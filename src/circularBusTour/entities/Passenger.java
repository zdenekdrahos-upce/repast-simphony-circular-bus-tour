package circularBusTour.entities;

public class Passenger {

    private double incomingTime, busArrivalTime;
    private int offAtStop;

    public Passenger(int getOffAtStop) {
        this.offAtStop = getOffAtStop;
    }

    public void setIncomingTime(double incomingTime) {
        this.incomingTime = incomingTime;
    }

    public void setBusArrivalTime(double busArrivalTime) {
        this.busArrivalTime = busArrivalTime;
    }

    public double getWaitingTime() {
        return busArrivalTime - incomingTime;
    }

    public boolean isGetOffAtStop(int idStop) {
        return offAtStop == idStop;
    }

    public void changeToNextBus(double timeGetOff) {
        incomingTime = timeGetOff - getWaitingTime();
    }
}
