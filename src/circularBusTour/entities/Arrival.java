package circularBusTour.entities;

public class Arrival {

    private double ticks, delay;

    public void setTime(double arrivalTime) {
        ticks = arrivalTime;
    }

    public void increaseTime(double ticks) {
        this.ticks += ticks; 
    }

    public double getTime() {
        return ticks + delay;
    }

    public void delay(double otherDeparture) {
        if (isPreceding(otherDeparture)) {            
            delay += otherDeparture - getTime();
        }
    }

    public boolean isPreceding(double otherDeparture) {
        return getTime() < otherDeparture;
    }
    
    public double getDelay() {
        return delay;
    }

    public void addDelayToTime() {
        ticks += delay;
        delay = 0;
    }

    public void addPreviousDelay(double previousDelay) {
        ticks -= previousDelay;
        delay += previousDelay;
    }
}
