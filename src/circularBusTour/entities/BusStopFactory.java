package circularBusTour.entities;

public interface BusStopFactory {

    BusStop create(int id);
    
}
