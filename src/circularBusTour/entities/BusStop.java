package circularBusTour.entities;

import java.util.Queue;
import java.util.LinkedList;

public class BusStop {

    private int id;
    private Queue<Passenger> waitingPassengers = new LinkedList<Passenger>();
    
    public BusStop(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public boolean areThereWaitingPassengers() {
        return getNumberOfWaitingPassengers() > 0;
    }

    private int getNumberOfWaitingPassengers() {
        return waitingPassengers.size();
    }

    public void pushPassenger(Passenger passenger) {
        waitingPassengers.offer(passenger);
    }

    public Passenger popPassenger() {
        if (areThereWaitingPassengers()) {
            return waitingPassengers.poll();
        }
        throw new NoPassengersAtStop();
    }

    public static class NoPassengersAtStop extends RuntimeException {}
}
