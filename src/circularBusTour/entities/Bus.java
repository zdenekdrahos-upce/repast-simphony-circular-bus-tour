package circularBusTour.entities;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Bus implements Iterable<Passenger> {

    private int id, capacity;
    private List<Passenger> passengers = new ArrayList<Passenger>();

    public Bus(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public void getOn(Passenger passenger) {
        if (isCapacityOverflowed()) {
            throw new FullCapacity();            
        }
        passengers.add(passenger);
    }

    private boolean isCapacityOverflowed() {
        return !isCapacitySet() || areAllSeatsTaken();
    }

    public boolean isFull() {
        return isCapacitySet() && areAllSeatsTaken();
    }

    private boolean isCapacitySet() {
        return capacity > 0;
    }

    private boolean areAllSeatsTaken() {
        return capacity == passengers.size();
    }

    @Override
    public Iterator<Passenger> iterator() {
        return passengers.iterator();
    }

    public double getOccupancyRate() {
        if (isCapacitySet()) {
            return 100 * passengers.size() /  (double) capacity;
        }
        return 0;
    }

    public static class FullCapacity extends RuntimeException {}
}
