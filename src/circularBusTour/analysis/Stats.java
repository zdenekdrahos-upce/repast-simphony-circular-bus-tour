package circularBusTour.analysis;

public class Stats {

    private double min, max, count, sum;

    public Stats() {
        reset();
    }

    public final void reset() {
        sum = 0;
        count = 0;
        min = Double.MAX_VALUE;
        max = Double.NEGATIVE_INFINITY;
    }

    public void add(double value) {
        count++;
        sum += value;
        min = Math.min(min, value);
        max = Math.max(max, value);
    }

    public double getMin() {
        return existsOneOrMore() ? min : 0;
    }

    public double getAvg() {
        return existsOneOrMore() ? (sum / count) : 0;
    }

    public double getMax() {
        return existsOneOrMore() ? max : 0;
    }

    private boolean existsOneOrMore() {
        return count > 0;
    }
}
