package circularBusTour.analysis;

import circularBusTour.busatstop.BusAtStopStats;

public class BusAtStopAnalyzer {

    private int delaysCount, statsCount;
    private final Stats delays = new Stats();
    private final Stats waitingTimes = new Stats();
    private final Stats occupancyRates = new Stats();

    public BusAtStopAnalyzer() {
        reset();
    }

    public final void reset() {
        delaysCount = 0;
        statsCount = 0;
        delays.reset();
        waitingTimes.reset();
        occupancyRates.reset();
    }

    public void analyze(BusAtStopStats s) {
        statsCount++;
        delays.add(s.arrivalDelay);
        if (s.isNotArrivalToTerminus()) {
            occupancyRates.add(s.busOccupancyRate);
        }
        addWaitingTimes(s);
        updateDelaysCount(s.arrivalDelay);
    }

    private void addWaitingTimes(BusAtStopStats s) {
        for (double waitingTime : s.waitingTimes) {
            waitingTimes.add(waitingTime);
        }
    }

    private void updateDelaysCount(double arrivalDelay) {
        if (arrivalDelay > 0) {
            delaysCount++;
        }
    }

    public Stats getDelays() {
        return delays;
    }

    public Stats getOccupancyRates() {
        return occupancyRates;
    }

    public int getDelaysCount() {
        return delaysCount;
    }

    public int getAnalyzedStatsCount() {
        return statsCount;
    }

    public Stats getWaitingTimes() {
        return waitingTimes;
    }

}
