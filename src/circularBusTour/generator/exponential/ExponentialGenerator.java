package circularBusTour.generator.exponential;

import repast.simphony.random.RandomHelper;
import cern.jet.random.Exponential;
import circularBusTour.generator.DurationGenerator;

public class ExponentialGenerator implements DurationGenerator {

    private final Exponential exponentialGenerator;

    public ExponentialGenerator(double mean) {
        double lambda = 1 / mean;
        exponentialGenerator = RandomHelper.createExponential(lambda);
    }

    public double next() {
        return exponentialGenerator.nextDouble();
    }

}
