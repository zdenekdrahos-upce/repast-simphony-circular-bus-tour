package circularBusTour.generator;

public interface DurationGenerator {

    public double next();
    
}
