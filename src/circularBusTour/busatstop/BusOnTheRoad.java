package circularBusTour.busatstop;

import java.util.List;

import circularBusTour.busatstop.stoptype.BusToTerminus;
import circularBusTour.entities.Arrival;
import circularBusTour.entities.Bus;
import circularBusTour.entities.BusStop;
import circularBusTour.entities.Passenger;

public class BusOnTheRoad {

    private final Bus bus;
    private Arrival arrival;
    private BusStop stop;
    private StopType stopType;
    private double departureTime;
    
    public BusOnTheRoad(Bus bus, Arrival arrival) {
        this.bus = bus;
        this.arrival = arrival;
    }

    public void setStopType(StopType stopType) {
        this.stopType = stopType;
    }

    public void setStop(BusStop stop) {
        this.stop = stop;
    }

    public List<Passenger> getOn() {
        return stopType.getOn(bus, stop);
    }

    public List<Passenger> getOff() {
        List<Passenger> passengers = stopType.getOff(bus, stop);
        if (stopType instanceof BusToTerminus) {
            for (Passenger p : passengers) {
                if (!p.isGetOffAtStop(0)) {
                    p.changeToNextBus(arrival.getTime());
                    stop.pushPassenger(p);
                }
            }
        }
        return passengers;
    }

    public double getArrivalTime() {
        return arrival.getTime();
    }

    public double getArrivalDelay() {
        return arrival.getDelay();
    }

    public double getOccupancyRate() {
        return bus.getOccupancyRate();
    }

    public int getIdBus() {
        return bus.getId();
    }

    public int getIdStop() {
        return stop.getId();
    }

    public void setDepartureTime(double departureTime) {
        this.departureTime = departureTime;
        arrival.setTime(departureTime);
        arrival.addDelayToTime();
    }

    public void delay(double departureOfOtherBus) {
        arrival.delay(departureOfOtherBus);
    }

    public void addTimeToNextArrival(double time) {
        arrival.increaseTime(time);
    }

    public void addPreviousDelay(double previousDelay) {
        arrival.addPreviousDelay(previousDelay);
    }

    public double getDepartureTime() {
        return departureTime;
    }
}
