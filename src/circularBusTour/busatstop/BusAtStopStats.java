package circularBusTour.busatstop;

import java.util.ArrayList;
import java.util.List;

public class BusAtStopStats {

    public int idBus;
    public int idStop;
    public double arrivalTime;
    public double departureTime;
    public double arrivalDelay;
    public double busOccupancyRate;
    public final List<Double> waitingTimes = new ArrayList<Double>();

    public boolean isNotArrivalToTerminus() {
        return !(idStop == 0 && busOccupancyRate == 0 && waitingTimes.isEmpty() && arrivalTime > 0);
    }
}