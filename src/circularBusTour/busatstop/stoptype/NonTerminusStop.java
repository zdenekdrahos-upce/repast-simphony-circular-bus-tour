package circularBusTour.busatstop.stoptype;

import java.util.List;

import circularBusTour.busatstop.StopType;
import circularBusTour.entities.Bus;
import circularBusTour.entities.BusStop;
import circularBusTour.entities.Passenger;

public class NonTerminusStop implements StopType {

    private final GetOnAndOffHelper getOnOff = new GetOnAndOffHelper();

    public List<Passenger> getOff(Bus bus, BusStop stop) {
        return getOnOff.getOff(bus, stop);
    }

    public List<Passenger> getOn(Bus bus, BusStop stop) {
        return getOnOff.getOn(bus, stop);
    }
}
