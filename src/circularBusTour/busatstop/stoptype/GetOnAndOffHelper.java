package circularBusTour.busatstop.stoptype;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import circularBusTour.entities.Bus;
import circularBusTour.entities.BusStop;
import circularBusTour.entities.Passenger;

class GetOnAndOffHelper {

    private final List<Passenger> noPassengers = new ArrayList<Passenger>();

    public List<Passenger> getNoPassengers() {
        return noPassengers;
    }

    public List<Passenger> everybodyGetOff(Bus bus) {
        Iterator<Passenger> it = bus.iterator();
        List<Passenger> getOff = new ArrayList<>();        
        while (it.hasNext()) {
            Passenger p = it.next();
            getOff.add(p);
            it.remove();
        }
        return getOff;
    }

    public List<Passenger> getOff(Bus bus, BusStop stop) {
        Iterator<Passenger> it = bus.iterator();
        List<Passenger> getOff = new ArrayList<>();        
        while (it.hasNext()) {
            Passenger p = it.next();
            if (p.isGetOffAtStop(stop.getId())) {
                getOff.add(p);
                it.remove();
            }
        }
        return getOff;
    }

    public List<Passenger> getOn(Bus bus, BusStop stop) {
        List<Passenger> passengers = new ArrayList<Passenger>();
        while (existsPassengerWhoCanGetOn(bus, stop)) {
            Passenger p = getPassengerOnTheBus(bus, stop);
            passengers.add(p);
        }
        return passengers;
    }

    private boolean existsPassengerWhoCanGetOn(Bus bus, BusStop stop) {
        return !bus.isFull() && stop.areThereWaitingPassengers();
    }

    private Passenger getPassengerOnTheBus(Bus bus, BusStop stop) {
        Passenger p = stop.popPassenger();
        bus.getOn(p);
        return p;
    }
}
