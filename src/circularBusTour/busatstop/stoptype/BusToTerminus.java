package circularBusTour.busatstop.stoptype;

import java.util.List;

import circularBusTour.busatstop.StopType;
import circularBusTour.entities.Bus;
import circularBusTour.entities.BusStop;
import circularBusTour.entities.Passenger;

public class BusToTerminus implements StopType {

    private final GetOnAndOffHelper getOnOff = new GetOnAndOffHelper();
    
    public List<Passenger> getOff(Bus bus, BusStop stop) {
        return getOnOff.everybodyGetOff(bus);
    }

    public List<Passenger> getOn(Bus bus, BusStop stop) {
        return getOnOff.getNoPassengers();
    }

}
