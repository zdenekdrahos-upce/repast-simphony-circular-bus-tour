package circularBusTour.busatstop;

import circularBusTour.generator.DurationGenerator;
import circularBusTour.TourRequest.BusAtStopRequest;

class PassengersStopwatch {

    private int timeGetOn, timeGetOff;
    private final DurationGenerator generatorOn, generatorOff;
    
    public PassengersStopwatch(BusAtStopRequest request) {
        generatorOn = getGenerator(request.on);
        generatorOff = getGenerator(request.off);
    }

    private DurationGenerator getGenerator(DurationGenerator userGenerator) {
        return userGenerator != null ? userGenerator : ZeroGenerator.generator;
    }

    public void getOff(int passengersCount) {
        timeGetOff = 0;
        for (int i = 0; i < passengersCount; i++) {
            timeGetOff += generatorOff.next();
        }
    }

    public void getOn(int passengersCount) {
        timeGetOn = 0;
        for (int i = 0; i < passengersCount; i++) {
            timeGetOn += generatorOn.next();
        }
    }

    public double getDurationOfStop() {
        return Math.max(timeGetOn, timeGetOff);
    }

    private static class ZeroGenerator implements DurationGenerator {

        private final static ZeroGenerator generator = new ZeroGenerator();
        
        @Override
        public double next() {
            return 0;
        }
    }
}
