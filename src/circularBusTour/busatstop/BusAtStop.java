package circularBusTour.busatstop;

import java.util.List;

import circularBusTour.TourRequest.BusAtStopRequest;
import circularBusTour.entities.Passenger;

public class BusAtStop {

    private BusAtStopStats stats;
    private PassengersStopwatch passengersStopwatch;

    public void prepareTrackingPassengersAtStop(BusAtStopRequest request) {
        passengersStopwatch = new PassengersStopwatch(request);
    }

    public void fulfilPassengersNeeds(BusOnTheRoad busOnTheRoad) {
        List<Passenger> outgoing = busOnTheRoad.getOff();
        List<Passenger> incoming = busOnTheRoad.getOn();
        
        passengersStopwatch.getOff(outgoing.size());
        passengersStopwatch.getOn(incoming.size());
        
        stats = new BusAtStopStats();
        loadStats(busOnTheRoad);
        loadPassengersWaitingTimes(incoming);
        
        busOnTheRoad.setDepartureTime(stats.departureTime);        
    }

    public BusAtStopStats getLastStats() {
        return stats;
    }

    private void loadStats(BusOnTheRoad busOnTheRoad) {
        stats.idBus = busOnTheRoad.getIdBus();
        stats.idStop = busOnTheRoad.getIdStop();
        stats.arrivalTime = busOnTheRoad.getArrivalTime();
        stats.arrivalDelay = busOnTheRoad.getArrivalDelay();
        stats.busOccupancyRate = busOnTheRoad.getOccupancyRate();
        stats.departureTime = busOnTheRoad.getArrivalTime() + passengersStopwatch.getDurationOfStop();
    }

    private void loadPassengersWaitingTimes(List<Passenger> incoming) {
        for (Passenger p : incoming) {
            p.setBusArrivalTime(stats.arrivalTime);
            stats.waitingTimes.add(p.getWaitingTime());
        }
    }
    
}
