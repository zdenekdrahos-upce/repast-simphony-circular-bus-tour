package circularBusTour.busatstop;

import java.util.List;

import circularBusTour.entities.Bus;
import circularBusTour.entities.BusStop;
import circularBusTour.entities.Passenger;

public interface StopType {

    List<Passenger> getOff(Bus bus, BusStop stop);

    List<Passenger> getOn(Bus bus, BusStop stop);
}
