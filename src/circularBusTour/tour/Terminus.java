package circularBusTour.tour;

import java.util.LinkedList;
import java.util.Queue;

import repast.simphony.engine.schedule.IAction;
import circularBusTour.busatstop.BusAtStopStats;
import circularBusTour.entities.Arrival;
import circularBusTour.OneTimeScheduler;

class Terminus implements IAction {

    private final int departureInterval;
    private final OneTimeScheduler scheduler;
    private final Arrival arrival = new Arrival();
    private boolean isDepartureMissed, isDelayed;
    private final Queue<ChangeBusStop> waitingBuses = new LinkedList<ChangeBusStop>();

    public Terminus(OneTimeScheduler api, int interval) {
        scheduler = api;
        isDelayed = false;
        isDepartureMissed = false;
        departureInterval = interval;
    }

    public void parkBus(ChangeBusStop bus) {
        waitingBuses.offer(bus);
        if (isDepartureMissed) {
            sendBusBackToTourAfterMissedTerminusDeparture();
        }
    }

    private void sendBusBackToTourAfterMissedTerminusDeparture() {
        sendBus();
        isDepartureMissed = false;
    }
    
    public void sendBus() {
        ChangeBusStop firstRide = waitingBuses.poll();
        dispatchBusFromTerminus(firstRide);
    }

    @Override
    public void execute() {
        if (isDelayed) {
            isDelayed = false;
            scheduleArrival(); 
        } else if (existsParkedBus()) {
            ChangeBusStop action = waitingBuses.poll();
            action.loadArrivalToTerminus(arrival);
            dispatchBusFromTerminus(action);                
            arrival.addDelayToTime();
        } else {
            isDepartureMissed = true;
        }
    }

    private boolean existsParkedBus() {
        return !waitingBuses.isEmpty();
    }

    private void dispatchBusFromTerminus(ChangeBusStop action) {
        action.leaveTerminus();            
        arrival.setTime(action.getDepartureTime());
        arrival.increaseTime(departureInterval);
        scheduleArrival();
    }

    private void scheduleArrival() {
        scheduler.schedule(arrival.getTime(), this);
    }

    public void checkDelay(BusAtStopStats stats) {
        if (isTerminusDepartureDelayed(stats)) {
            delay(stats.departureTime);
        }
    }

    private boolean isTerminusDepartureDelayed(BusAtStopStats stats) {
        return !stats.isNotArrivalToTerminus() && arrival.isPreceding(stats.departureTime);
    }

    private void delay(double departureTimeOfOtherBus) {
        arrival.delay(departureTimeOfOtherBus);
        isDelayed = true;
    }
}
