package circularBusTour.tour;

import repast.simphony.engine.schedule.IAction;
import circularBusTour.busatstop.BusOnTheRoad;
import circularBusTour.busatstop.StopType;
import circularBusTour.busatstop.stoptype.*;
import circularBusTour.entities.Arrival;
import circularBusTour.entities.BusStop;
import circularBusTour.tour.BusApi;

class ChangeBusStop implements IAction {

    private static StopType busFromTerminus = new BusFromTerminus();
    private static StopType busToTerminus = new BusToTerminus();
    private static StopType classicStop = new NonTerminusStop();
    
    private final BusApi busApi;
    private final int timeBetweenStops;
    private final BusOnTheRoad busOnTheRoad;
    
    public ChangeBusStop(BusApi busApi, BusOnTheRoad busOnTheRoad, int timeBetweenStops) {
        this.busApi = busApi;
        this.busOnTheRoad = busOnTheRoad;
        this.timeBetweenStops = timeBetweenStops;
    }

    public void leaveTerminus() {
        processBus(busFromTerminus);
    }

    @Override
    public void execute() {
        if (busApi.isStillOtherBusAtStop(busOnTheRoad)) {
            processDelayedBus();
        } else if (isAtTerminus()) {
            processArrivedBus(busToTerminus);
            busApi.parkBus(this);
        } else {
            processBus(classicStop);
        }
    }

    private void processDelayedBus() {
        double departureTimeOfOtherBus = busApi.getLastDepartureFromStop(busOnTheRoad.getIdStop());
        busOnTheRoad.delay(departureTimeOfOtherBus);
        scheduleArrival();
    }

    private void processArrivedBus(StopType stopType) {
        busOnTheRoad.setStopType(stopType);            
        busApi.executeStop(busOnTheRoad);
    }

    private void processBus(StopType stopType) {
        processArrivedBus(stopType);                
        prepareNextStop();
        scheduleArrival();
    }

    private void prepareNextStop() {      
        BusStop next = busApi.getNextStop(busOnTheRoad);
        busOnTheRoad.setStop(next);
        busOnTheRoad.addTimeToNextArrival(timeBetweenStops);        
    }

    private void scheduleArrival() {
        busApi.schedule(busOnTheRoad.getArrivalTime(), this);
    }

    private boolean isAtTerminus() {
        return busOnTheRoad.getIdStop() == 0;
    }

    public double getDepartureTime() {
        return busOnTheRoad.getDepartureTime();
    }

    public void loadArrivalToTerminus(Arrival arrival) {
        busOnTheRoad.setDepartureTime(arrival.getTime());                
        if (arrival.getDelay() > 0) {
            busOnTheRoad.addPreviousDelay(arrival.getDelay());
        }
    }
}