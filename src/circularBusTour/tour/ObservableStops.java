package circularBusTour.tour;

import circularBusTour.busatstop.BusAtStopStats;
import circularBusTour.observer.BusAtStopObserver;

class ObservableStops {

    private BusAtStopObserver observers[] = {};
    
    public void register(BusAtStopObserver[] observers) {
        if (observers != null) {
            this.observers = observers;
        }
    }

    public void notify(BusAtStopStats stats) {
        for (BusAtStopObserver o : observers) {
            o.update(stats);
        }
    }

}
