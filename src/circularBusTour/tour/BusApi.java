package circularBusTour.tour;

import repast.simphony.engine.schedule.IAction;
import circularBusTour.busatstop.BusOnTheRoad;
import circularBusTour.entities.BusStop;

interface BusApi {

    BusStop getNextStop(BusOnTheRoad busOnTheRoad);

    boolean isStillOtherBusAtStop(BusOnTheRoad busOnTheRoad);

    double getLastDepartureFromStop(int idStop);
 
    void executeStop(BusOnTheRoad busOnTheRoad);

    void parkBus(ChangeBusStop changeStop);

    void schedule(double arrival, IAction action);
}
