package circularBusTour.tour;

import repast.simphony.engine.schedule.IAction;
import circularBusTour.busatstop.BusAtStop;
import circularBusTour.busatstop.BusAtStopStats;
import circularBusTour.busatstop.BusOnTheRoad;
import circularBusTour.buses.Buses;
import circularBusTour.OneTimeScheduler;
import circularBusTour.stops.Stops;
import circularBusTour.TourRequest.BusIntervalsRequest;
import circularBusTour.entities.Arrival;
import circularBusTour.entities.Bus;
import circularBusTour.entities.BusStop;
import circularBusTour.observer.BusAtStopObserver;

public class TourRunner implements BusApi {

    private final Buses buses;
    private final Stops stops;
    private final BusAtStop busAtStop;
    private final ObservableStops observable = new ObservableStops();
    private Terminus terminus;
    private OneTimeScheduler scheduler;
    private BusAtStopStats lastStopStats;
    
    public TourRunner(Buses buses, Stops stops, BusAtStop busAtStop) {        
        this.buses = buses;
        this.stops = stops;
        this.busAtStop = busAtStop;
    }
    
    public void setScheduler(OneTimeScheduler scheduler) {
        this.scheduler = scheduler;
    }

    public void prepareTerminusAndBusArrivals(BusIntervalsRequest time) {
        loadTerminus(time.terminusDeparture);
        loadBuses(time.timeBetweenStops);
    }

    private void loadTerminus(int terminusDeparture) {
        terminus = new Terminus(scheduler, terminusDeparture);
    }

    private void loadBuses(int timeBetweenStops) {
        BusStop terminusStop = stops.getTerminus();
        for (Bus bus : buses) {
            BusOnTheRoad busOnTheRoad = new BusOnTheRoad(bus, new Arrival());
            busOnTheRoad.setStop(terminusStop);
            
            ChangeBusStop change = new ChangeBusStop(this, busOnTheRoad, timeBetweenStops);
            terminus.parkBus(change);
        }
    }

    public void registerObservers(BusAtStopObserver[] observers) {
        observable.register(observers);
    }

    public void sendFirstBusFromTerminus() {
        terminus.sendBus();
    }

    @Override
    public BusStop getNextStop(BusOnTheRoad busOnTheRoad) {
        return stops.getNextStop(busOnTheRoad.getIdStop());
    }

    @Override
    public boolean isStillOtherBusAtStop(BusOnTheRoad busOnTheRoad) {        
        return stops.isOtherBusAtStop(busOnTheRoad.getIdStop(), busOnTheRoad.getArrivalTime());
    }

    @Override
    public double getLastDepartureFromStop(int idStop) {
        return stops.getLastDeparture(idStop);
    }
    
    @Override
    public void executeStop(BusOnTheRoad busOnTheRoad) {
        checkCollisionAtStop(busOnTheRoad);        
        processBusAtStop(busOnTheRoad);        
        stops.setLastDeparture(lastStopStats.idStop, lastStopStats.departureTime);
        terminus.checkDelay(lastStopStats);
        observable.notify(lastStopStats);
    }

    private void checkCollisionAtStop(BusOnTheRoad busOnTheRoad) {
        if (isStillOtherBusAtStop(busOnTheRoad)) {
            throw new TimeCollisionAtStop();
        }
    }

    private void processBusAtStop(BusOnTheRoad busOnTheRoad) {
        buses.busArrivedToStop(busOnTheRoad.getIdBus(), busOnTheRoad.getIdStop());
        busAtStop.fulfilPassengersNeeds(busOnTheRoad);
        lastStopStats = busAtStop.getLastStats();
    }

    @Override
    public void parkBus(ChangeBusStop changeStop) {
        terminus.parkBus(changeStop);
    }

    @Override
    public void schedule(double arrival, IAction action) {
        scheduler.schedule(arrival, action);
    }

    public static class TimeCollisionAtStop extends RuntimeException {
    }
}
