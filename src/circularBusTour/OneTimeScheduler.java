package circularBusTour;

import repast.simphony.engine.schedule.IAction;

public interface OneTimeScheduler {

    void schedule(double arrival, IAction action);

    double getCurrentTick();
    
}
