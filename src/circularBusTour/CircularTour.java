package circularBusTour;

import circularBusTour.busatstop.BusAtStop;
import circularBusTour.buses.Buses;
import circularBusTour.stops.Stops;
import circularBusTour.tour.TourRunner;

public class CircularTour {

    private final Buses buses = new Buses();
    private final Stops stops = new Stops();
    private final BusAtStop busAtStop = new BusAtStop();
    private final TourRunner tour = new TourRunner(buses, stops, busAtStop);
    
    public CircularTour(OneTimeScheduler scheduler) {
        tour.setScheduler(scheduler);
        stops.setScheduler(scheduler);
    }

    public void execute(TourRequest request) {
        loadTourFromRequest(request);
        stops.startGeneratingPassengers();
        tour.sendFirstBusFromTerminus();
    }

    private void loadTourFromRequest(TourRequest request) {
        buses.parkBusesAtTerminus(request.bus);
        stops.prepareStopsAndPassengers(request.stop);
        busAtStop.prepareTrackingPassengersAtStop(request.busAtStop);
        tour.prepareTerminusAndBusArrivals(request.interval);
        tour.registerObservers(request.observers);
    }

    public int getLastBusStop(int busIndex) {
        return buses.getLastStop(busIndex);
    }
}
