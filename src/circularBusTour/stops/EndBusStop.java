package circularBusTour.stops;

import repast.simphony.random.RandomHelper;

class EndBusStop {

    private final int busStopsCount;
    
    public EndBusStop(int busStopsCount) {
        this.busStopsCount = busStopsCount;        
    }

    public int generate(int from) {
        int to;
        do {
            to = generateStopId();
        } while (to == from);
        return to;
    }

    private int generateStopId() {
        return RandomHelper.nextIntFromTo(0, busStopsCount - 1);
    }

}
