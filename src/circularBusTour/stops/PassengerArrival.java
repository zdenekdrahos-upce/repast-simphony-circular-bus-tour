package circularBusTour.stops;

import circularBusTour.generator.DurationGenerator;
import circularBusTour.entities.BusStop;
import circularBusTour.entities.Passenger;

class PassengerArrival {

    private final BusStop busStop;
    private final TimeIntervals intervals;
    private final EndBusStop endBusStop;

    public PassengerArrival(DurationGenerator generator, BusStop busStop, EndBusStop endStops) {
        this.busStop = busStop;
        this.endBusStop = endStops;
        intervals = new TimeIntervals(generator);
    }

    public void addPassenger(double currentTime) {
        int from = busStop.getId();
        int to = endBusStop.generate(from);
        Passenger p = new Passenger(to);
        p.setIncomingTime(currentTime);
        busStop.pushPassenger(p);
    }

    public double getArrivalOfNextPassenger() {
        return intervals.generate();
    }
}
