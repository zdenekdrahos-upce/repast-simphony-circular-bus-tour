package circularBusTour.stops;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import circularBusTour.OneTimeScheduler;
import circularBusTour.TourRequest.StopRequest;
import circularBusTour.entities.BusStop;
import circularBusTour.entities.BusStopFactory;

public class Stops {

    private final List<BusStop> busStops = new ArrayList<BusStop>();
    private final PassengersGenerator passengersGenerator = new PassengersGenerator();
    private final Map<Integer, Double> lastDepartures = new HashMap<Integer, Double>();
    
    public void setScheduler(OneTimeScheduler scheduler) {
        passengersGenerator.setScheduler(scheduler);
    }

    public void prepareStopsAndPassengers(StopRequest request) {        
        createBusStops(request.count, request.factory);
        passengersGenerator.setDuration(request.incomingPassengers);
    }

    private void createBusStops(int stopsCount, BusStopFactory factory) {
        for (int i = 0; i < stopsCount; i++) {
            BusStop s = factory.create(i);
            busStops.add(s);
            lastDepartures.put(i, 0.0);
        }
    }

    public void startGeneratingPassengers() {
        passengersGenerator.scheduleGeneratingPassengers(busStops);
    }

    public BusStop getNextStop(int idCurrentStop) {
        int idNext = isLastStop(idCurrentStop) ? 0 : idCurrentStop + 1;
        return busStops.get(idNext);
    }

    private boolean isLastStop(int idCurrentStop) {
        return idCurrentStop == (busStops.size() - 1);
    }

    public BusStop getTerminus() {
        return busStops.get(0);
    }

    public double getLastDeparture(int idStop) {
        return lastDepartures.get(idStop); 
    }

    public void setLastDeparture(int idStop, double lastDeparture) {
        lastDepartures.put(idStop, lastDeparture);
    }

    public boolean isOtherBusAtStop(int idStop, double arrival) {
        return arrival < getLastDeparture(idStop);
    }

}
