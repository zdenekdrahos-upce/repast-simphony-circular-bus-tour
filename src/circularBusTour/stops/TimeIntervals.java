package circularBusTour.stops;

import circularBusTour.generator.DurationGenerator;

class TimeIntervals {

    private double currentTick;
    private final DurationGenerator exponentialGenerator;
    
    public TimeIntervals(DurationGenerator generator) {
        this.currentTick = 0;
        exponentialGenerator = generator;
    }

    public double generate() {
        currentTick += exponentialGenerator.next();
        return currentTick;
    }
}
