package circularBusTour.stops;

import java.util.List;

import repast.simphony.engine.schedule.IAction;
import circularBusTour.generator.DurationGenerator;
import circularBusTour.OneTimeScheduler;
import circularBusTour.entities.BusStop;

class PassengersGenerator {

    private EndBusStop endBusStop;
    private OneTimeScheduler scheduler;
    private DurationGenerator duration;

    public void setDuration(DurationGenerator incomingPassengers) {
        duration = incomingPassengers;        
    }

    public void setScheduler(OneTimeScheduler scheduler) {
        this.scheduler = scheduler;
    }
    
    public void scheduleGeneratingPassengers(List<BusStop> stops) {        
        if (isGeneratorSet()) {
            endBusStop = new EndBusStop(stops.size());
            for (BusStop s : stops) {
                PassengerArrival a = new PassengerArrival(duration, s, endBusStop);
                new RepeatingActionRunner(a).scheduleNextTick();
            }
        }
    }

    private boolean isGeneratorSet() {
        return duration != null;
    }

    private class RepeatingActionRunner implements IAction {

        private final PassengerArrival passengers;

        public RepeatingActionRunner(PassengerArrival generator) {
            this.passengers = generator;
        }
        
        @Override
        public void execute() {
            double currentTick = scheduler.getCurrentTick();
            passengers.addPassenger(currentTick);
            scheduleNextTick();
        }

        private void scheduleNextTick() {
            double tick = passengers.getArrivalOfNextPassenger();
            scheduler.schedule(tick, this);
        }
    }
}
