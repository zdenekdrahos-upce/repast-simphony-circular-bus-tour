package circularBusTour.animation.area;

import java.util.ArrayList;
import java.util.List;

import repast.simphony.space.graph.Network;

public class RoadNetwork {

    private final Network<ElementInArea> network;
    private final List<Road> roads = new ArrayList<>();

    public RoadNetwork(Network<ElementInArea> network) {
        this.network = network;
    }

    public void addRoad(Road road) {
        roads.add(road);
        network.addEdge(road);
    }

    public Road getNextRoad(Road current) {
        for (Road r : roads) {
            if (r.isNext(current)) {
                return r;
            }
        }
        throw new InvalidLastStop();
    }

    public Road getRoadToTerminus() {
        return roads.get(roads.size() - 1);
    }

    public static class InvalidLastStop extends RuntimeException {
    }
}
