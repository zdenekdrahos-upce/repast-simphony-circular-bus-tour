package circularBusTour.animation.area;

import repast.simphony.space.SpatialMath;
import repast.simphony.space.continuous.ContinuousSpace;
import repast.simphony.space.continuous.NdPoint;

public class Area {

    private final ContinuousSpace<ElementInArea> area;
    
    public Area(ContinuousSpace<ElementInArea> area) {
        this.area = area;
    }

    public void moveObjectToStop(ElementInArea object, AnimatedStop stop) {
        area.moveTo(object, stop.getX(), stop.getY());
    }

    public void moveBus(AnimatedBus b) {
        area.moveByVector(b, b.getMoveStep(), b.getAngle(), 0);
    }

    public double getRoadAngle(Road r) {
        return getAngle(r.getSource(), r.getTarget());
    }

    private double getAngle(ElementInArea from, ElementInArea to) {
        NdPoint locationFrom = area.getLocation(from);
        NdPoint locationTo = area.getLocation(to);
        return SpatialMath.calcAngleFor2DMovement(area, locationFrom, locationTo);
    }
}
