package circularBusTour.animation.area;


public class AnimatedStop implements ElementInArea {

    private int peopleCount;
    private int[] coordinates;
    
    public AnimatedStop(int[] coordinates) {
        peopleCount = 0;
        this.coordinates = coordinates;
    }

    public int getNumberOfWaitingPassengers() {
        return peopleCount;
    }

    public double getDistance(AnimatedStop to) {
        double x = getX() - to.getX();
        double y = getY() - to.getY();
        return Math.sqrt(x*x + y*y);
    }
    
    public int getX() {
        return coordinates[0];
    }

    public int getY() {
        return coordinates[1];
    }

    public void incrementPassengers() {
        peopleCount++;
    }

    public void decrementPassengers() {
        peopleCount--;
    }    
}
