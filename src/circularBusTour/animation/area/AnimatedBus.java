package circularBusTour.animation.area;


public class AnimatedBus implements ElementInArea {

    private Road road;
    private int occupancyRate = 0;
    private double currentDistance, maxDistance, angle, moveStep;

    public AnimatedBus(Road firstRoad) {
        road = firstRoad;
    }

    public void setRoad(Road r) {
        this.road = r;
    }
    
    public Road getRoad() {
        return road;
    }

    public void setOccupancyRate(double rate) {
        occupancyRate = (int)Math.round(rate);
    }

    public String getLabel() {
        return String.format("%d%%", occupancyRate);
    }

    public void prepareMove(double distance, double angle, double time) {
        currentDistance = 0;
        maxDistance = distance;
        this.angle = angle;
        this.moveStep = distance / time;
    }

    public void move() {
        currentDistance += getMoveStep();
    }

    public boolean isOnTheRoad() {
        return currentDistance < maxDistance;
    }

    public double getAngle() {
        return angle;
    }

    public double getMoveStep() {
        return moveStep;
    }
}
