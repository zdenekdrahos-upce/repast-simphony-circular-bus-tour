package circularBusTour.animation.area;

import repast.simphony.space.graph.RepastEdge;

public class Road extends RepastEdge<ElementInArea> implements ElementInArea {

    private double distance;
    
    public Road(AnimatedStop from, AnimatedStop to) {
        super(from, to, false);
        distance = from.getDistance(to);
    }

    public double getDistance() {
        return distance;
    }

    public boolean isNext(Road current) {
        return current.getTarget() == this.getSource();
    }
    
}
