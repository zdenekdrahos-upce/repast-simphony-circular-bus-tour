package circularBusTour.animation;

import circularBusTour.animation.area.AnimatedStop;
import circularBusTour.animation.area.ElementInArea;
import circularBusTour.animation.area.Road;
import circularBusTour.animation.area.RoadNetwork;
import repast.simphony.context.space.graph.NetworkBuilder;
import repast.simphony.space.graph.Network;

class CircularRoadBuilder {

    private RoadNetwork network;
    
    public RoadNetwork build(NetworkBuilder<ElementInArea> netBuilder, Iterable<AnimatedStop> stops) {
        createNetwork(netBuilder);
        addRoads(stops);
        return network;
    }

    private void createNetwork(NetworkBuilder<ElementInArea> netBuilder) {
        Network<ElementInArea> repastNetwork = netBuilder.buildNetwork();
        network = new RoadNetwork(repastNetwork);
    }

    private void addRoads(Iterable<AnimatedStop> stops) {
        AnimatedStop firstStop = null, from = null, to = null;
        for (AnimatedStop s : stops) {
            if (firstStop == null) {
                firstStop = s;
                from = s;
            } else {
                to = s;
                addRoad(from, to);
                from = to;
            }
        }
        addRoad(from, firstStop);
    }

    private void addRoad(AnimatedStop from, AnimatedStop to) {
        Road road = new Road(from, to);
        network.addRoad(road);
    }
}
