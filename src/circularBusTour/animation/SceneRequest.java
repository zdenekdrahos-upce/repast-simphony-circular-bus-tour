package circularBusTour.animation;

import circularBusTour.animation.area.ElementInArea;
import repast.simphony.context.Context;

public class SceneRequest {

    public int busesCount, stopsCount, distanceBetweenStops;
    public double areaWidth, areaHeight;
    public Context<ElementInArea> context;
    
}
