package circularBusTour.animation;

import java.util.ArrayList;

import circularBusTour.animation.area.AnimatedBus;
import circularBusTour.animation.area.AnimatedStop;
import circularBusTour.animation.area.Area;
import circularBusTour.animation.area.ElementInArea;
import repast.simphony.context.space.continuous.ContinuousSpaceFactory;
import repast.simphony.context.space.continuous.ContinuousSpaceFactoryFinder;
import repast.simphony.context.space.graph.NetworkBuilder;
import repast.simphony.space.continuous.ContinuousSpace;
import repast.simphony.space.continuous.SimpleCartesianAdder;
import repast.simphony.space.continuous.StrictBorders;

public class SceneBuilder {

    private SceneRequest request;
    private SceneResponse response;

    private final CircularRoadBuilder roadBuilder = new CircularRoadBuilder();
    private final RectangleLayout stopsLayout = new RectangleLayout();
    
    public SceneResponse build(SceneRequest request) {
        response = new SceneResponse();
        this.request = request;
        buildArea(); 
        addStops();
        addRoads();
        addBuses();
        return response;
    }

    private void buildArea() {
        ContinuousSpaceFactory factory = ContinuousSpaceFactoryFinder.createContinuousSpaceFactory(null);
        ContinuousSpace<ElementInArea> space = factory.createContinuousSpace(
                "citySpace", request.context, new SimpleCartesianAdder<ElementInArea>(),
                new StrictBorders(), request.areaWidth, request.areaHeight
        );
        response.area = new Area(space);
    }

    private void addStops() {
        response.stops = new ArrayList<>();
        stopsLayout.loadCoordinates(request.stopsCount, request.distanceBetweenStops);
        for (int i = 0; i < request.stopsCount; i++) {
            AnimatedStop s = new AnimatedStop(stopsLayout.getCoordinates(i));
            response.stops.add(s);
            addObjectToContext(s);
            response.area.moveObjectToStop(s, s);
        }
    }

    private void addRoads() {
        NetworkBuilder<ElementInArea> netBuilder = new NetworkBuilder<ElementInArea>("roads", request.context, true);
        response.roads = roadBuilder.build(netBuilder, response.stops);
    }

    private void addBuses() {
        response.buses  = new ArrayList<>();
        for (int i = 0; i < request.busesCount; i++) {
            addBus();
        }
    }

    private void addBus() {
        AnimatedBus b = new AnimatedBus(response.roads.getRoadToTerminus());
        response.buses.add(b);
        addObjectToContext(b);
        AnimatedStop terminus = response.stops.get(0);
        response.area.moveObjectToStop(b, terminus);
    }

    private void addObjectToContext(ElementInArea e) {
        request.context.add(e);
    }
}
