package circularBusTour.animation;

import java.util.List;

import circularBusTour.animation.area.AnimatedBus;
import circularBusTour.animation.area.AnimatedStop;
import circularBusTour.animation.area.Area;
import circularBusTour.animation.area.RoadNetwork;

public class SceneResponse {

    public Area area;
    public RoadNetwork roads;
    public List<AnimatedBus> buses;
    public List<AnimatedStop> stops;
    
}
