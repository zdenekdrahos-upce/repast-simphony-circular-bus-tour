package circularBusTour.animation;

class RectangleLayout {

    private int[][] coordinates;

    public int[] getCoordinates(int id) {        
        return coordinates[id];
    }
    
    public void loadCoordinates(int objectsCount, int innerDistance) {
        coordinates = new int[objectsCount][2];
        int firstQuarter = objectsCount / 4;
        int halfStop = objectsCount / 2;
        int thirdQuarter = 3 * objectsCount / 4; 
        
        int x = innerDistance;
        int y = innerDistance;
        
        for (int i = 0; i < objectsCount; i++) {

            if (i <= firstQuarter) {
                x += innerDistance;
            } else if (i >= halfStop && i <= thirdQuarter) {
                x -= innerDistance;
            }

            if (i >= firstQuarter && i <= halfStop) {
                y += innerDistance;
            } else if (i >= thirdQuarter) {
                y -= innerDistance;
            }  

            coordinates[i][0] = x;
            coordinates[i][1] = y;
        }
    }
    
}
