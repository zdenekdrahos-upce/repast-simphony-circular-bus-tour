package circularBusTour.observer;

import java.util.ArrayList;
import java.util.List;

import circularBusTour.analysis.BusAtStopAnalyzer;
import circularBusTour.busatstop.BusAtStopStats;

public class StatsStorage implements BusAtStopObserver {

    private final List<BusAtStopStats> stats = new ArrayList<BusAtStopStats>();
    private final BusAtStopAnalyzer analyzer = new BusAtStopAnalyzer();

    @Override
    public void update(BusAtStopStats lastStopStats) {
        stats.add(lastStopStats);
        analyzer.analyze(lastStopStats);
    }

    public List<BusAtStopStats> getStats() {
        return stats;
    }

    public BusAtStopAnalyzer getAnalyzer() {
        return analyzer;
    }

    public void reset() {
        analyzer.reset();
    }
}
