package circularBusTour.observer;

import circularBusTour.busatstop.BusAtStopStats;

public interface BusAtStopObserver {

    void update(BusAtStopStats lastStopStats);
    
}
