package circularBusTour.buses;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import circularBusTour.TourRequest.BusRequest;
import circularBusTour.entities.Bus;

public class Buses implements Iterable<Bus> {

    private final List<Bus> buses  = new ArrayList<Bus>();
    private final Map<Integer, Integer> lastStops = new HashMap<Integer, Integer>();    
    
    public void parkBusesAtTerminus(BusRequest request) {
        for (int i = 0; i < request.count; i++) {
            addBus(i, request.capacity);
        }
    }

    private void addBus(int idBus, int capacity) {
        Bus b = new Bus(idBus);
        b.setCapacity(capacity);
        parkBusAtTerminus(b);
        buses.add(b);
    }

    private void parkBusAtTerminus(Bus b) {
        lastStops.put(b.getId(), 0);
    }

    public int getLastStop(int idBus) {
        return lastStops.get(idBus);
    }

    public void busArrivedToStop(int idBus, int idStop) {
        lastStops.put(idBus, idStop);
    }

    @Override
    public Iterator<Bus> iterator() {
        return buses.iterator();
    }
}
