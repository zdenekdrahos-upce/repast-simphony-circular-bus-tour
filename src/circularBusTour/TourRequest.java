package circularBusTour;

import circularBusTour.entities.BusStopFactory;
import circularBusTour.entities.factory.BasicBusStopFactory;
import circularBusTour.generator.DurationGenerator;
import circularBusTour.observer.BusAtStopObserver;

public class TourRequest {

    public final BusRequest bus = new BusRequest();
    public final StopRequest stop = new StopRequest();
    public final BusAtStopRequest busAtStop = new BusAtStopRequest();
    public final BusIntervalsRequest interval = new BusIntervalsRequest();
    public BusAtStopObserver observers[] = {};
    
    public static class BusRequest {
        public int count;
        public int capacity;
    }
    
    public static class StopRequest {
        public int count;
        public DurationGenerator incomingPassengers;
        public BusStopFactory factory = new BasicBusStopFactory();
    }
    
    public static class BusAtStopRequest {
        public DurationGenerator on;
        public DurationGenerator off;
    }
    
    public static class BusIntervalsRequest {
        public int terminusDeparture;
        public int timeBetweenStops;
    }
    
}
