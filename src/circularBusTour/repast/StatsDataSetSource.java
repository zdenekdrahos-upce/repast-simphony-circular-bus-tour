package circularBusTour.repast;

import circularBusTour.TourRequest;
import circularBusTour.analysis.BusAtStopAnalyzer;

public class StatsDataSetSource {

    private final TourRequest request;
    private final BusAtStopAnalyzer analyzer;

    public StatsDataSetSource(TourRequest request, BusAtStopAnalyzer analyzer) {
        this.request = request;
        this.analyzer = analyzer;
    }
    
    public double getBusCount() {
        return request.bus.count;
    }
    
    public double getTerminusInterval() {
        return request.interval.terminusDeparture;
    }
    
    public double getAnalyzedStatsCount() {
        return analyzer.getAnalyzedStatsCount();
    }
    
    public double getDelaysCount() {
        return analyzer.getDelaysCount();
    }
    
    public double getMinWaitingTime() {
        return analyzer.getWaitingTimes().getMin() / 60.0;
    }
    
    public double getAvgWaitingTime() {
        return analyzer.getWaitingTimes().getAvg() / 60.0;
    }
    
    public double getMaxWaitingTime() {
        return analyzer.getWaitingTimes().getMax() / 60.0;
    }
    
    public double getMinOccupancyRate() {
        return analyzer.getOccupancyRates().getMin();
    }
    
    public double getAvgOccupancyRate() {
        return analyzer.getOccupancyRates().getAvg();
    }
    
    public double getMaxOccupancyRate() {
        return analyzer.getOccupancyRates().getMax();
    }

    public double getMinDelay() {
        return analyzer.getDelays().getMin() / 60.0;
    }
    
    public double getAvgDelay() {
        return analyzer.getDelays().getAvg() / 60.0;
    }
    
    public double getMaxDelay() {
        return analyzer.getDelays().getMax() / 60.0;
    }
}
