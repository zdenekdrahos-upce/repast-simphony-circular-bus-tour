package circularBusTour.repast.animation;

import repast.simphony.context.Context;
import repast.simphony.engine.controller.NullAbstractControllerAction;
import repast.simphony.engine.environment.RunEnvironmentBuilder;
import repast.simphony.engine.environment.RunState;
import repast.simphony.engine.environment.GUIRegistryType;
import repast.simphony.parameter.Parameters;
import repast.simphony.scenario.ModelInitializer;
import repast.simphony.scenario.Scenario;

public class LogDisplayInitializer implements ModelInitializer {

    public static final LogDisplay display = new LogDisplay();

    @SuppressWarnings("rawtypes")
    public void initialize(Scenario scen, RunEnvironmentBuilder builder) {

        scen.addMasterControllerAction(new NullAbstractControllerAction() {

            @Override
            public void runInitialize(RunState runState, Context context, Parameters runParams) {
                runState.getGUIRegistry().addDisplay("LOG", GUIRegistryType.OTHER, display);
            }

            @Override
            public void runCleanup(RunState runState, Context context) {
                display.destroy();
            }

            public String toString() {
                return "Create LOG display";
            }
        });
    }
}