package circularBusTour.repast.animation;

import java.util.List;

import circularBusTour.animation.area.AnimatedStop;
import circularBusTour.entities.BusStop;
import circularBusTour.entities.BusStopFactory;
import circularBusTour.entities.Passenger;

class AnimationBusStopFactory implements BusStopFactory {

    private final List<AnimatedStop> stops;
    
    public AnimationBusStopFactory(List<AnimatedStop> stops) {
        this.stops = stops;
    }

    @Override
    public BusStop create(int id) {
        AnimatedStop a = stops.get(id);
        return new BusStopUpdater(id, a);
    }

    private class BusStopUpdater extends BusStop {
        
        private AnimatedStop animatedStop;
        
        public BusStopUpdater(int id, AnimatedStop animatedStop) {
            super(id);
            this.animatedStop = animatedStop;
        }

        @Override
        public void pushPassenger(Passenger passenger) {
            super.pushPassenger(passenger);
            animatedStop.incrementPassengers();
        }

        @Override
        public Passenger popPassenger() {
            animatedStop.decrementPassengers();
            return super.popPassenger();
        }
    }
}
