package circularBusTour.repast.animation;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JToolBar;

import circularBusTour.busatstop.BusAtStopStats;
import circularBusTour.observer.BusAtStopObserver;
import circularBusTour.repast.StatsDataSetSource;
import repast.simphony.visualization.DisplayEditorLifecycle;
import repast.simphony.visualization.DisplayListener;
import repast.simphony.visualization.IDisplay;
import repast.simphony.visualization.Layout;
import repast.simphony.visualization.ProbeListener;

@SuppressWarnings("deprecation")
public class LogDisplay implements IDisplay, BusAtStopObserver {

    private final JPanel panel = new JPanel();
    private final JTextArea textArea = new JTextArea();

    public LogDisplay() {
        textArea.setEditable(false);
        panel.setLayout(new BorderLayout());
        panel.add(new JScrollPane(textArea), BorderLayout.CENTER);
    }
    
    @Override
    public void render() {
        textArea.repaint();
    }

    @Override
    public void update() {
    }

    @Override
    public void update(BusAtStopStats lastStopStats) {
        String line = convertStatsToString(lastStopStats);
        textArea.append(line);
        render();
    }

    private String convertStatsToString(BusAtStopStats s) {
        String line = "B" +  s.idBus + ": S" + s.idStop + " - "
                + s.arrivalTime + " - " + s.departureTime
                + ": occupancy rate = " + s.busOccupancyRate
                + "%, delay = " + s.arrivalDelay + "\n";
        return line;
    }

    public void appendSummary(StatsDataSetSource dataset) {
        String analysis = "\nAnalysis:";
        analysis += "\n- delays: " + dataset.getDelaysCount() + " / " + dataset.getAnalyzedStatsCount();
        analysis += "\n- delay: min = " + dataset.getMinDelay() + 
                ", avg = " + dataset.getAvgDelay() + ", max = " + dataset.getMaxDelay();
        analysis += "\n- occupancy rate: min = " + dataset.getMinOccupancyRate() + 
                ", avg = " + dataset.getAvgOccupancyRate() + ", max = " + dataset.getMaxOccupancyRate();
        analysis += "\n- waiting times: min = " + dataset.getMinWaitingTime() + 
                ", avg = " + dataset.getAvgWaitingTime() + ", max = " + dataset.getMaxWaitingTime();
        textArea.append(analysis);
    }

    @Override
    public JPanel getPanel() {        
        return panel;
    }

    @Override
    public void destroy() {
        textArea.setText("");
    }
    
    @Override
    public void setPause(boolean pause) {
    }

    @Override
    public void init() {
    }

    @Override
    public void setLayout(Layout<?, ?> layout) {        
    }

    @Override
    public void addDisplayListener(DisplayListener listener) {
    }

    @Override
    public void setLayoutFrequency(LayoutFrequency frequency, int interval) {
    }


    @Override
    public void registerToolBar(JToolBar bar) {
    }

    @Override
    public void iconified() {
    }

    @Override
    public void deIconified() {
    }

    @Override
    public void closed() {
    }

    @Override
    public void addProbeListener(ProbeListener listener) {
    }

    @Override
    public Layout<?, ?> getLayout() {
        return null;
    }

    @Override
    public DisplayEditorLifecycle createEditor(JPanel panel) {
        return null;
    }

    @Override
    public void resetHomeView() {
    }
    
}
