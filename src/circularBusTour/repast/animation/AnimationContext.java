package circularBusTour.repast.animation;

import repast.simphony.context.DefaultContext;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.schedule.ISchedule;
import repast.simphony.engine.schedule.ScheduleParameters;
import circularBusTour.animation.SceneBuilder;
import circularBusTour.animation.SceneRequest;
import circularBusTour.animation.SceneResponse;
import circularBusTour.animation.area.AnimatedBus;
import circularBusTour.animation.area.ElementInArea;
import circularBusTour.animation.area.Road;
import circularBusTour.busatstop.BusAtStopStats;
import circularBusTour.entities.BusStopFactory;
import circularBusTour.observer.BusAtStopObserver;

public class AnimationContext extends DefaultContext<ElementInArea> implements BusAtStopObserver {

    private SceneResponse scene;
    private final SceneBuilder sceneBuilder = new SceneBuilder();
    private final SceneRequest request = new SceneRequest();

    public AnimationContext() {
        super("animationContext");     
        request.distanceBetweenStops = 120;
        request.areaWidth = 1000;
        request.areaHeight = 1000;
    }

    public void buildScene(int busesCount, int stopsCount) {
        request.context = this;
        request.busesCount = busesCount;
        request.stopsCount = stopsCount;
        scene = sceneBuilder.build(request);
    }

    @Override
    public void update(BusAtStopStats lastStopStats) {
        updateOccupancyRate(lastStopStats);
        if (lastStopStats.isNotArrivalToTerminus()) {            
            dispatchBusToNextStop(lastStopStats);
        }
    }

    private void updateOccupancyRate(BusAtStopStats lastStopStats) {
        AnimatedBus b = scene.buses.get(lastStopStats.idBus);
        b.setOccupancyRate(lastStopStats.busOccupancyRate);
    }

    private void dispatchBusToNextStop(BusAtStopStats lastStopStats) {
        AnimatedBus b = scene.buses.get(lastStopStats.idBus);
        Road current = b.getRoad();
        Road r = scene.roads.getNextRoad(current);
        b.setRoad(r);
        
        double distance = r.getDistance();
        double angle = scene.area.getRoadAngle(r);

        scheduleNextBusMove(b, lastStopStats.departureTime);
        b.prepareMove(distance, angle, request.distanceBetweenStops);
    }

    private void scheduleNextBusMove(AnimatedBus b, double departureTime) {
        ISchedule schedule = RunEnvironment.getInstance().getCurrentSchedule();
        ScheduleParameters params = ScheduleParameters.createOneTime(departureTime + 1);
        schedule.schedule(params, this, "moveBus", b);
    }
    
    public void moveBus(AnimatedBus b) {               
        b.move();
        scene.area.moveBus(b);        
        if (b.isOnTheRoad()) {
            ISchedule schedule = RunEnvironment.getInstance().getCurrentSchedule();
            scheduleNextBusMove(b, schedule.getTickCount());
        }
    }

    public BusStopFactory getStopsFactory() {
        return new AnimationBusStopFactory(scene.stops);
    }
}
