package circularBusTour.repast;

import circularBusTour.OneTimeScheduler;
import repast.simphony.engine.schedule.IAction;
import repast.simphony.engine.schedule.ISchedule;
import repast.simphony.engine.schedule.ScheduleParameters;

public class Scheduler implements OneTimeScheduler {

    private ISchedule schedule;

    public Scheduler(ISchedule schedule) {
        this.schedule = schedule;
    }

    @Override
    public void schedule(double tick, IAction action) {
        ScheduleParameters when = ScheduleParameters.createOneTime(tick);
        schedule.schedule(when, action);
    }

    @Override
    public double getCurrentTick() {
        return schedule.getTickCount();
    }
}
