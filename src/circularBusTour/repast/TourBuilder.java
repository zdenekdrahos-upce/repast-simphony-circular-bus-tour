package circularBusTour.repast;

import circularBusTour.CircularTour;
import circularBusTour.OneTimeScheduler;
import circularBusTour.TourRequest;
import circularBusTour.entities.factory.BasicBusStopFactory;
import circularBusTour.generator.exponential.ExponentialGenerator;
import circularBusTour.observer.BusAtStopObserver;
import circularBusTour.observer.StatsStorage;
import circularBusTour.repast.animation.AnimationContext;
import circularBusTour.repast.animation.LogDisplayInitializer;
import repast.simphony.context.Context;
import repast.simphony.dataLoader.ContextBuilder;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.schedule.ISchedule;
import repast.simphony.engine.schedule.ScheduleParameters;
import repast.simphony.parameter.Parameters;

public class TourBuilder implements ContextBuilder<Object> {

    private Parameters params;
    private ISchedule schedule;
    private Context<Object> context;

    private final TourRequest request = new TourRequest();
    private final StatsStorage stats = new StatsStorage();
    private final StatsDataSetSource dataset = new StatsDataSetSource(request, stats.getAnalyzer());
    
    public TourBuilder() {
        request.interval.terminusDeparture = 500;
        request.interval.timeBetweenStops = 120;
        request.bus.count = 7;
        request.bus.capacity = 120;
        request.stop.count = 20;
        request.stop.incomingPassengers = new ExponentialGenerator(100);
        request.busAtStop.on = new ExponentialGenerator(5);
        request.busAtStop.off = new ExponentialGenerator(5);        
    }
    
    @Override
    public Context<Object> build(Context<Object> context) {        
        loadEnvironment(context);
        loadSimulation();        
        runTour();
        scheduleEndOfSimulation();
        return context;
    }

    private void loadEnvironment(Context<Object> context) {
        stats.reset();
        this.context = context;
        params = RunEnvironment.getInstance().getParameters();
        schedule = RunEnvironment.getInstance().getCurrentSchedule();
    }

    private void loadSimulation() {
        request.bus.count = params.getInteger("buses");
        request.stop.count = params.getInteger("stops");
        request.stop.factory = new BasicBusStopFactory();
        request.observers = new BusAtStopObserver[] {stats};
        request.interval.terminusDeparture = params.getInteger("terminusInterval");
        if (params.getBoolean("animation")) {
            loadAnimation();
        }
    }

    private void loadAnimation() {
        AnimationContext animation = new AnimationContext();
        animation.buildScene(request.bus.count, request.stop.count);
        context.addSubContext(animation);
        request.observers = new BusAtStopObserver[] {stats, animation, LogDisplayInitializer.display};
        request.stop.factory = animation.getStopsFactory();
    }

    private void runTour() {
        OneTimeScheduler scheduler = new Scheduler(schedule);
        CircularTour tour = new CircularTour(scheduler);
        tour.execute(request);
    }

    private void scheduleEndOfSimulation() {
        int endTick = params.getInteger("endTick");
        ScheduleParameters params = ScheduleParameters.createOneTime(endTick);
        schedule.schedule(params, this, "endSimulation");
    }

    public void endSimulation() {                
        publishStats();
        RunEnvironment.getInstance().endRun();
    }

    private void publishStats() {
        LogDisplayInitializer.display.appendSummary(dataset);
        context.add(dataset);
    }
}
