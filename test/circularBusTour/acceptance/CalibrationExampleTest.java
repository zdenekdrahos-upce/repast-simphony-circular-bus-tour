package circularBusTour.acceptance;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import repast.simphony.context.DefaultContext;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.environment.RunState;
import repast.simphony.engine.schedule.Schedule;
import circularBusTour.busatstop.BusAtStopStats;
import circularBusTour.CircularTour;
import circularBusTour.OneTimeScheduler;
import circularBusTour.TourRequest;
import circularBusTour.analysis.BusAtStopAnalyzer;
import circularBusTour.analysis.Stats;
import circularBusTour.generator.exponential.ExponentialGenerator;
import circularBusTour.helpers.TourTestHelper;
import circularBusTour.observer.BusAtStopObserver;
import circularBusTour.observer.StatsStorage;
import circularBusTour.repast.Scheduler;

public class CalibrationExampleTest {

    private Schedule schedule;
    private CircularTour tour;
    private final TourRequest request = new TourRequest();
    private TourTestHelper tourTester;
    private final StatsStorage stats = new StatsStorage();
    
    @Before
    public void setUp() {
        schedule = new Schedule();
        RunEnvironment.init(schedule, null, null, true);
        RunState.init().setMasterContext(new DefaultContext<Object>());
        OneTimeScheduler scheduler = new Scheduler(schedule);
        tour = new CircularTour(scheduler);
        tourTester = new TourTestHelper(schedule, tour, request);
        request.observers = new BusAtStopObserver[] {stats};
    }

    @Test
    public void testEightStopsAndThreeBuses() {
        request.interval.terminusDeparture = 1200;
        request.interval.timeBetweenStops = 120;
        request.bus.count = 3;
        request.bus.capacity = 120;
        request.stop.count = 8;
        request.stop.incomingPassengers = new ExponentialGenerator(100);
        request.busAtStop.on = new ExponentialGenerator(5);
        request.busAtStop.off = new ExponentialGenerator(5);
        tour.execute(request);

        tourTester.simulateCirculars(5);
        printConflictBusStops();
        tourTester.assertThatNoTwoBusesAreAtSameStopAtSameTick();
        
        printAnalysis();
    }

    private void printConflictBusStops() {
        List<BusAtStopStats> stats = this.stats.getStats();
        for (BusAtStopStats s : stats) {            
            printBusAtStopStat(s);
            for (BusAtStopStats s2 : stats) {
                if (s != s2 && s.idStop == s2.idStop) {
                    if ((s2.arrivalTime >= s.departureTime || s.arrivalTime >= s2.departureTime) == false) {                        
                        printBusAtStopStat(s2);
                        System.out.println();
                    }
                }
            }
        }
    }

    private void printBusAtStopStat(BusAtStopStats s1) {
        System.out.println(s1.idBus + ": S" + s1.idStop + " - "
                + s1.arrivalTime + " - " + s1.departureTime
                + ": occupancy rate = " + s1.busOccupancyRate
                + "%, delay = " + s1.arrivalDelay);
    }

    private void printAnalysis() {
        BusAtStopAnalyzer analyzer = stats.getAnalyzer();
        System.out.println();
        System.out.println("Analysis:");
        System.out.println("- delays: " + analyzer.getDelaysCount() + " / " + analyzer.getAnalyzedStatsCount());
        printStat("delay", analyzer.getDelays());
        printStat("occupancy rate", analyzer.getOccupancyRates());
        printWaitingTimesInMinutes("waiting times", analyzer.getWaitingTimes());
    }

    private void printStat(String heading, Stats s) {
        System.out.println("- " + heading + ": min = " + s.getMin() + ", avg = " + s.getAvg() + ", max = " + s.getMax());
    }

    private void printWaitingTimesInMinutes(String heading, Stats s) {
        System.out.println("- " + heading + ": min = " + (s.getMin() / 60) + ", avg = " + (s.getAvg() / 60) + ", max = " + (s.getMax() / 60));        
    }

}
