package circularBusTour.stops;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import circularBusTour.generator.DurationGenerator;
import circularBusTour.entities.BusStop;
import circularBusTour.entities.Passenger;
import circularBusTour.mocks.OnePassengerPerSecond;

public class PassengerArrivalTest {

    private PassengerArrival arrival;
    private BusStop terminus;
    
    @Before
    public void setUp() {
        terminus = new BusStop(0);
        DurationGenerator generator = new OnePassengerPerSecond();
        EndBusStop endStops = new EndBusStop(10);
        arrival = new PassengerArrival(generator, terminus, endStops);
    }

    @Test
    public void passengersAreComingToTerminus() {
        arrival.addPassenger(0);            
        assertTrue(terminus.areThereWaitingPassengers());
    }

    @Test
    public void passengerHasIncomingTime() {
        arrival.addPassenger(10);
        Passenger p = terminus.popPassenger();
        p.setBusArrivalTime(100);
        assertEquals(90, p.getWaitingTime(), 0.0);
    }

    @Test
    public void passengerDontGetOffAtSameStopAsHeGetOn() {
        add100passengers();
        while (terminus.areThereWaitingPassengers()) {
            Passenger p = terminus.popPassenger();
            assertFalse(p.isGetOffAtStop(0));
        }
    }

    private void add100passengers() {
        for (int i = 0; i < 100; i++) {
            arrival.addPassenger(0);            
        }
    }

    @Test
    public void arrivalOfNextPassengersShouldBeIncreasing() {
        double current, previous = -1;        
        
        for (int i = 0; i < 100; i++) {
            current = arrival.getArrivalOfNextPassenger();
            assertTrue(current > previous);            
            previous = current;
        }
    }

}
