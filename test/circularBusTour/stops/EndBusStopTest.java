package circularBusTour.stops;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class EndBusStopTest {

    private final EndBusStop generator = new EndBusStop(8);
    
    @Test
    public void generatedStopIsDifferentThanInputStop() {
        int from = 0;
        int to = generator.generate(from);
        assertTrue(from != to);
    }

    @Test
    public void zeroIsMinIndex_itsGeneratedAtLeastOnce() {
        assertEquals(0, findMinGeneratedIndex(generator));
    }

    private int findMinGeneratedIndex(EndBusStop generator) {
        int from = 1, min = Integer.MAX_VALUE;
        for (int i = 0; i < 100; i++) {
            min = Math.min(min, generator.generate(from));
        }
        return min;
    }   

    @Test
    public void sevenIsMaxIndexFor8stops_itsGeneratedAtLeastOnce() {
        assertEquals(7, findMaxGeneratedIndex(generator));
    }

    private int findMaxGeneratedIndex(EndBusStop generator) {
        int from = 1, max = Integer.MIN_VALUE;
        for (int i = 0; i < 100; i++) {
            max = Math.max(max, generator.generate(from));
        }
        return max;
    }
}
