package circularBusTour.stops;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import circularBusTour.generator.DurationGenerator;
import circularBusTour.mocks.OnePassengerPerSecond;

public class TimeIntervalsTest {

    @Test
    public void everyTickShouldBeLargerThanPreviousTick() {
        DurationGenerator g = new OnePassengerPerSecond();
        TimeIntervals generator = new TimeIntervals(g);
        
        double current, previous = 0;
        for (int i = 0; i < 100; i++) {
            current = generator.generate();
            assertTrue(current > previous);
            previous = current;
        }
    }
}
