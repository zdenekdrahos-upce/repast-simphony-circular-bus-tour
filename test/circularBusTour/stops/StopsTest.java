package circularBusTour.stops;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import circularBusTour.TourRequest.StopRequest;
import circularBusTour.mocks.SaveAndCreateBusStop;

public class StopsTest {

    private final Stops stops = new Stops();

    private void createStops(int stopsCount) {
        StopRequest r = new StopRequest();
        r.count = stopsCount;
        stops.prepareStopsAndPassengers(r);
    }

    @Test
    public void twoStops_ifBusIsAtTerminusThenNextStopShouldHaveIdOne() {
        createStops(2);
        assertEquals(1, stops.getNextStop(0).getId());
    }

    @Test
    public void twoStops_ifBusIsAtSecondStopThenNextStopShouldBeTerminus() {
        createStops(2);
        assertEquals(0, stops.getNextStop(1).getId());
    }
    
    @Test
    public void factoryShouldBeUsedForCreatingStops() {
        SaveAndCreateBusStop factory = new SaveAndCreateBusStop();
        StopRequest r = new StopRequest();
        r.count = 2;
        r.factory = factory;
        stops.prepareStopsAndPassengers(r);
        
        assertEquals(r.count, factory.usedIds.size());
        assertEquals(0, (int)factory.usedIds.get(0));
        assertEquals(1, (int)factory.usedIds.get(1));
    }

    @Test
    public void lastDepartureForNewStopIsZero() {
        createStops(1);
        assertEquals(0, stops.getLastDeparture(0), 0.0);
    }

    @Test
    public void ifLastDepartureIsSetToTenThenGetLastDepartureShouldReturnTrue() {
        createStops(1);
        stops.setLastDeparture(0, 10);
        assertEquals(10, stops.getLastDeparture(0), 0.0);
    }

    @Test
    public void ifLastDepartureIsSetToTen_otherBusCannotArriveAt5() {
        createStops(1);
        stops.setLastDeparture(0, 10);
        assertTrue(stops.isOtherBusAtStop(0, 5));
    }

    @Test
    public void ifLastDepartureIsSetToTen_otherBusCanArriveAt10orMore() {
        createStops(1);
        stops.setLastDeparture(0, 10);
        assertFalse(stops.isOtherBusAtStop(0, 10));
    }
}
