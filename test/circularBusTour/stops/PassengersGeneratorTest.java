package circularBusTour.stops;

import static org.junit.Assert.assertFalse;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import repast.simphony.context.DefaultContext;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.environment.RunState;
import repast.simphony.engine.schedule.Schedule;
import circularBusTour.generator.DurationGenerator;
import circularBusTour.OneTimeScheduler;
import circularBusTour.entities.BusStop;
import circularBusTour.entities.Passenger;
import circularBusTour.mocks.OnePassengerPerSecond;
import circularBusTour.repast.Scheduler;

public class PassengersGeneratorTest {

    private final PassengersGenerator generator = new PassengersGenerator();
    private final List<BusStop> busStops = new ArrayList<>();
    private final Schedule schedule = new Schedule();
    
    @Before
    public void setUp() {
        for (int i = 0; i < 10; i++) {
            busStops.add(new BusStop(i));
        }
        DurationGenerator duration = new OnePassengerPerSecond();
        generator.setDuration(duration);
    }

    @Test
    public void passengerDontGetOffAtSameStopAsHeGetOn() {
        setScheduler();
        add100passengersToAllStops();
        
        for (BusStop stop : busStops) {
            int idStop = stop.getId();
            while (stop.areThereWaitingPassengers()) {
                Passenger p = stop.popPassenger();
                assertPassengerGetOffAddDifferentStop(p, idStop);
            }
        }
    }

    private void assertPassengerGetOffAddDifferentStop(Passenger p, int stopWhereGetOn) {
        assertFalse(p.isGetOffAtStop(stopWhereGetOn));
    }

    private void setScheduler() { 
        RunEnvironment.init(schedule, null, null, true);
        RunState.init().setMasterContext(new DefaultContext<Object>());
        OneTimeScheduler scheduler = new Scheduler(schedule);
        generator.setScheduler(scheduler);
    }

    private void add100passengersToAllStops() {
        setScheduler();
        generator.scheduleGeneratingPassengers(busStops);
        for (int i = 0; i < 100; i++) {
            schedule.execute();
        }
    }
}
