package circularBusTour.buses;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import circularBusTour.TourRequest.BusRequest;
import circularBusTour.entities.Bus;
import circularBusTour.entities.Passenger;

public class BusesTest {

    private final Buses buses = new Buses();
    private final BusRequest oneBusRequest = createBusWithCapacityOne();

    private BusRequest createBusWithCapacityOne() {
        BusRequest r = new BusRequest();
        r.count = 1;
        r.capacity = 1;
        return r;
    }

    @Test
    public void busesShouldBeParkedAtTerminus_theirLastStopShouldBeZero() {        
        buses.parkBusesAtTerminus(oneBusRequest);
        assertEquals(0, buses.getLastStop(0));
    }

    @Test
    public void ifBusIsSendToSecondStopThenLastStopShouldBeTwo() {
        buses.parkBusesAtTerminus(oneBusRequest);        
        buses.busArrivedToStop(0, 2);
        assertEquals(2, buses.getLastStop(0));
    }

    @Test
    public void ifBusesAreLoadedWithCapacityOneThenAfterAddingOnePassengerBusShouldBeFull() {
        buses.parkBusesAtTerminus(oneBusRequest);
        for (Bus b : buses) {
            assertBusHasCapacityOne(b);
        }
    }

    private void assertBusHasCapacityOne(Bus b) {
        assertFalse(b.isFull());
        b.getOn(new Passenger(0));
        assertTrue(b.isFull());
    }
}
