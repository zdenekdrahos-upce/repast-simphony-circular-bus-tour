package circularBusTour.busatstop;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import circularBusTour.TourRequest.BusAtStopRequest;
import circularBusTour.mocks.OnePassengerPerSecond;

public class PassengersStopwatchTest {

    private PassengersStopwatch stopwatch;

    @Before
    public void setUp() {
        BusAtStopRequest r = new BusAtStopRequest();
        r.on = new OnePassengerPerSecond();
        r.off = new OnePassengerPerSecond();
        stopwatch = new PassengersStopwatch(r);
    }

    @Test
    public void durationShouldBeZeroIfNobodyGetOnOrOff() {
        stopwatch.getOff(0);
        stopwatch.getOn(0);
        assertEquals(0, stopwatch.getDurationOfStop(), 0.0);
    }

    @Test
    public void getOnTwoAndGetOffOne_durationShouldBeTwo() {
        stopwatch.getOff(2);
        stopwatch.getOn(1);
        assertEquals(2, stopwatch.getDurationOfStop(), 0.0);
    }

    @Test
    public void getOnOneAndGetOffTwo_durationShouldBeTwo() {
        stopwatch.getOff(1);
        stopwatch.getOn(2);
        assertEquals(2, stopwatch.getDurationOfStop(), 0.0);
    }
}
