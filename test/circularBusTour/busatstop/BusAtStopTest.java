package circularBusTour.busatstop;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import circularBusTour.TourRequest.BusAtStopRequest;
import circularBusTour.busatstop.stoptype.*;
import circularBusTour.entities.Arrival;
import circularBusTour.entities.Bus;
import circularBusTour.entities.BusStop;
import circularBusTour.entities.Passenger;
import circularBusTour.mocks.OnePassengerPerSecond;

public class BusAtStopTest {

    private BusStop stopFrom, stopTo;    
    private Passenger passenger;
    
    private final Bus bus = new Bus(0);
    private final Arrival arrival = new Arrival();
    private final BusOnTheRoad busOnTheRoad = new BusOnTheRoad(bus, arrival);
    private final BusAtStop busAtStop = new BusAtStop();

    private BusAtStopStats stats;
    
    @Before
    public void setUp() {
        BusAtStopRequest r = new BusAtStopRequest();
        r.on = new OnePassengerPerSecond();
        r.off = new OnePassengerPerSecond();
        busAtStop.prepareTrackingPassengersAtStop(r);
        
        busOnTheRoad.setStopType(new NonTerminusStop());
        stopFrom = new BusStop(0);
        stopTo = new BusStop(1);
        bus.setCapacity(1);
        passenger = new Passenger(1);
    }

    private void loadStats(BusStop stop) {
        busOnTheRoad.setStop(stop);
        busAtStop.fulfilPassengersNeeds(busOnTheRoad);
        stats = busAtStop.getLastStats();
    }

    @Test
    public void testPassengerShouldHaveSetArrivalOfTheBus() {
        arrival.increaseTime(100);
        
        passenger.setIncomingTime(20);
        stopFrom.pushPassenger(passenger);

        loadStats(stopFrom);        
        
        assertEquals(80, passenger.getWaitingTime(), 0.0);
    }

    @Test
    public void ifOnePassengerGetOffAndTwoGetOnThenOccupancyRateShouldBe20() {
        bus.setCapacity(10);
        bus.getOn(passenger);
        
        stopTo.pushPassenger(passenger);
        stopTo.pushPassenger(passenger);
        
        loadStats(stopTo);

        assertEquals(20, stats.busOccupancyRate, 0.0);
    }

    @Test
    public void passengerWaitingTimeOnArrivalShouldBeAvailableInStats() {
        passenger.setIncomingTime(0);
        arrival.increaseTime(100);
        
        stopFrom.pushPassenger(passenger);        
        loadStats(stopFrom);

        assertEquals(1, stats.waitingTimes.size());
        assertEquals(100, stats.waitingTimes.get(0), 0.0);
    }

    @Test
    public void ifEmptyBusArrivesAtTerminusThenArrivalTimeShouldEqualsArrivalTime() {                
        busOnTheRoad.setStop(stopFrom);
        busOnTheRoad.setStopType(new BusToTerminus());
        
        loadStats(stopFrom);        
        
        assertEquals(stats.arrivalTime, stats.departureTime, 0.0);
    }


    @Test
    public void testGetOffTheBus() {        
        bus.getOn(passenger);
        
        assertBusIsFull();
        loadStats(stopTo);
        assertBusIsEmpty();
    }

    @Test
    public void testGetOnTheBus() {
        stopFrom.pushPassenger(passenger);

        assertBusIsEmpty();
        assertAtLeastOneWaitingPassengerAtStopFrom();
        loadStats(stopFrom);      
        assertBusIsFull();
        assertNoPassengersAtStopFrom();
    }

    private void assertBusIsEmpty() {
        assertFalse(bus.isFull());
    }

    private void assertBusIsFull() {
        assertTrue(bus.isFull());
    }

    private void assertNoPassengersAtStopFrom() {
        assertFalse(stopFrom.areThereWaitingPassengers());
    }

    private void assertAtLeastOneWaitingPassengerAtStopFrom() {
        assertTrue(stopFrom.areThereWaitingPassengers());
    }

    @Test
    public void getOnTwoAndGetOffOne_departureTimeShouldBeTwo() {
        getOnTwo_getOffOne();
        assertEquals(2, stats.departureTime - stats.arrivalTime, 0.0);
    }

    @Test
    public void getOnTwoAndGetOffOneDelay10_busDepartureTimeShouldBeTwelveAndDelayShouldBeReseted() {
        busOnTheRoad.delay(10);
        getOnTwo_getOffOne();
        assertEquals(12, busOnTheRoad.getDepartureTime(), 0.0);
        assertEquals(0, busOnTheRoad.getArrivalDelay(), 0.0);
    }

    @Test
    public void getOnTwoAndGetOffOne_twoWaitingTimes() {
        getOnTwo_getOffOne();
        assertEquals(2, stats.waitingTimes.size(), 0.0);
    }

    private void getOnTwo_getOffOne() {
        bus.setCapacity(2);

        Passenger off = new Passenger(1);
        bus.getOn(off);
        
        Passenger on = new Passenger(2);
        stopTo.pushPassenger(on);
        stopTo.pushPassenger(on);
        loadStats(stopTo);
    }

    @Test
    public void ifBusArrivesAtTerminusThenNobodyGetOnAndEverybodyShouldGetOff() {        
        bus.getOn(passenger);
        stopFrom.pushPassenger(passenger);
        
        busOnTheRoad.setStop(stopFrom);
        busOnTheRoad.setStopType(new BusToTerminus());
        
        loadStats(stopFrom);
        assertBusIsEmpty();
    }
}
