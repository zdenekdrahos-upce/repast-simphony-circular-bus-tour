package circularBusTour.busatstop;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import circularBusTour.busatstop.stoptype.BusToTerminus;
import circularBusTour.entities.*;
import circularBusTour.mocks.NoStop;

public class BusOnTheRoadTest {

    private final Bus bus = new Bus(0);
    private final BusStop stop = new BusStop(0);
    private final Arrival arrival = new Arrival();
    private final BusOnTheRoad busOnTheRoad = new BusOnTheRoad(bus, arrival);

    @Before
    public void setUp() {
        busOnTheRoad.setStop(stop);
        busOnTheRoad.setStopType(new NoStop());
    }

    @Test
    public void testGetOn() {
        List<Passenger> passengers = busOnTheRoad.getOn();
        assertTrue(passengers.isEmpty());
    }

    @Test
    public void testGetOff() {
        List<Passenger> passengers = busOnTheRoad.getOff();
        assertTrue(passengers.isEmpty());
    }

    @Test
    public void passengerWhoChangeBusAtTerminusShouldWaitAtStopAndIncludePreviousWaitingTime() {
        Passenger passenger = getPassengerWhoAlreadyWaited20TicksAtPreviousStop();
        
        bus.setCapacity(1);   
        bus.getOn(passenger);
        arrival.increaseTime(100);
        
        busOnTheRoad.setStopType(new BusToTerminus());
        busOnTheRoad.getOff();        
        
        // he must wait on the stop
        assertTrue(stop.areThereWaitingPassengers());
        
        // if he gets on the next bus - then waiting time must include previous waiting time + waiting time at terminus        
        passenger.setBusArrivalTime(200);
        assertEquals(120, passenger.getWaitingTime(), 0.0);        
    }

    private Passenger getPassengerWhoAlreadyWaited20TicksAtPreviousStop() {
        Passenger passenger = new Passenger(1);
        passenger.setIncomingTime(0);
        passenger.setBusArrivalTime(20);
        assertEquals(20, passenger.getWaitingTime(), 0.0);
        return passenger;
    }

    @Test
    public void testGetArrivalTime() {
        arrival.increaseTime(200);
        assertEquals(200, busOnTheRoad.getArrivalTime(), 0.0);
    }

    @Test
    public void testGetDelay() {
        arrival.increaseTime(200);
        assertEquals(0, busOnTheRoad.getArrivalDelay(), 0.0);
        arrival.delay(220);
        assertEquals(20, busOnTheRoad.getArrivalDelay(), 0.0);
    }

    @Test
    public void testGetIdBus() {
        assertEquals(0, busOnTheRoad.getIdBus());
    }

    @Test
    public void testGetIdStop() {
        assertEquals(0, busOnTheRoad.getIdStop());
    }

    @Test
    public void testGetOccupancyRate() {
        bus.setCapacity(2);
        assertEquals(0, busOnTheRoad.getOccupancyRate(), 0.0);
        bus.getOn(new Passenger(0));
        assertEquals(50, busOnTheRoad.getOccupancyRate(), 0.0);
    }

    @Test
    public void testSetDepartureTime() {
        arrival.increaseTime(100);
        busOnTheRoad.setDepartureTime(120);        
        assertEquals(120, busOnTheRoad.getArrivalTime(), 0.0);
    }

    @Test
    public void testDelaying() {
        arrival.increaseTime(100);
        busOnTheRoad.delay(200);        
        assertEquals(100, busOnTheRoad.getArrivalDelay(), 0.0);
    }

    @Test
    public void testAddTime() {
        arrival.increaseTime(100);
        busOnTheRoad.addTimeToNextArrival(20);        
        assertEquals(120, busOnTheRoad.getArrivalTime(), 0.0);
    }

    @Test
    public void addDelayButDontChangeDepartureTime() {
        busOnTheRoad.addTimeToNextArrival(100);
        busOnTheRoad.addPreviousDelay(20);
        assertEquals(100, busOnTheRoad.getArrivalTime(), 0.0);
        assertEquals(20, busOnTheRoad.getArrivalDelay(), 0.0);
    }
}
