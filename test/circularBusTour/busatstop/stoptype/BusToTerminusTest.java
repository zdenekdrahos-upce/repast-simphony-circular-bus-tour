package circularBusTour.busatstop.stoptype;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import circularBusTour.entities.Bus;
import circularBusTour.entities.BusStop;
import circularBusTour.entities.Passenger;

public class BusToTerminusTest {

    private final BusToTerminus stopType = new BusToTerminus();
    private final Bus bus = new Bus(0);

    @Test
    public void everybodyGetOffTheBus() {        
        bus.setCapacity(1);
        bus.getOn(new Passenger(0));
        BusStop stop = new BusStop(0);

        stopType.getOff(bus, stop);
        assertBusIsEmpty();
    }

    @Test
    public void nobodyGetOnTheBus() {        
        BusStop stop = new BusStop(0);
        stop.pushPassenger(new Passenger(1));
        
        stopType.getOn(bus, stop);
        assertBusIsEmpty();
    }

    private void assertBusIsEmpty() {
        assertEquals(0, bus.getOccupancyRate(), 0.0);
    }
}
