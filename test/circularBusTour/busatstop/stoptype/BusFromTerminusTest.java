package circularBusTour.busatstop.stoptype;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

import circularBusTour.entities.Bus;
import circularBusTour.entities.BusStop;
import circularBusTour.entities.Passenger;

public class BusFromTerminusTest {

    private final BusFromTerminus stopType = new BusFromTerminus();
    private final Bus bus = new Bus(0);

    @Test
    public void nobodyGetOffTheBus() {
        BusStop stop = new BusStop(0);
        
        List<Passenger> passengers = stopType.getOff(bus, stop);
        assertEquals(0, passengers.size());
    }

    @Test
    public void busIsFullIfThereAreWaitingPassengersAtStop() {
        bus.setCapacity(1);
        BusStop stop = new BusStop(0);
        stop.pushPassenger(new Passenger(1));
        stop.pushPassenger(new Passenger(2));
        
        stopType.getOn(bus, stop);
        assertTrue(bus.isFull());
    }
}
