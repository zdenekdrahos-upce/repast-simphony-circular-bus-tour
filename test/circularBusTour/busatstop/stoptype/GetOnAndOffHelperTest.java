package circularBusTour.busatstop.stoptype;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

import circularBusTour.entities.Bus;
import circularBusTour.entities.BusStop;
import circularBusTour.entities.Passenger;

public class GetOnAndOffHelperTest {

    private final GetOnAndOffHelper helper = new GetOnAndOffHelper();

    @Test
    public void emptyListShouldBeReturnedIfForNoPassengers() {
        List<Passenger> passengers = helper.getNoPassengers();
        assertTrue(passengers.isEmpty());
    }

    @Test
    public void ifEverybodyGetOffThenBusShouldBeEmpty() {
        Bus bus = new Bus(0);
        bus.setCapacity(2);
        bus.getOn(new Passenger(0));
        bus.getOn(new Passenger(0));

        List<Passenger> passengers = helper.everybodyGetOff(bus);
        assertEquals(0, bus.getOccupancyRate(), 0.0);
        assertEquals(2, passengers.size());
    }

    @Test
    public void getOffOnlyPassengersWhoWantToGetOffAtStop1() {
        Bus bus = new Bus(0);
        bus.setCapacity(2);
        bus.getOn(new Passenger(1));
        bus.getOn(new Passenger(2));
        
        BusStop stop = new BusStop(1);
        
        List<Passenger> passengers = helper.getOff(bus, stop);
        assertEquals(1, passengers.size());
        for (Passenger p : passengers) {
            assertTrue(p.isGetOffAtStop(1));
        }
    }

    @Test
    public void ifBusHasCapacityOneThenOnlyOnePassengerGetOn() {
        Bus bus = new Bus(0);
        bus.setCapacity(1);
        BusStop stop = new BusStop(0);
        stop.pushPassenger(new Passenger(1));
        stop.pushPassenger(new Passenger(2));
        
        List<Passenger> passengers = helper.getOn(bus, stop);
        assertTrue(bus.isFull());
        assertEquals(1, passengers.size());
    }

    @Test
    public void ifBusIsFullThenNobodyCanGetOn() {
        Bus bus = new Bus(0);
        bus.setCapacity(1);
        bus.getOn(new Passenger(1));
        BusStop stop = new BusStop(0);
        stop.pushPassenger(new Passenger(1));
        stop.pushPassenger(new Passenger(2));
        
        List<Passenger> passengers = helper.getOn(bus, stop);
        assertEquals(0, passengers.size());
    }

    @Test
    public void ifNobodyWaitAtStopThenNobodyGetOn() {
        Bus bus = new Bus(0);
        bus.setCapacity(1);
        BusStop stop = new BusStop(0);
        
        List<Passenger> passengers = helper.getOn(bus, stop);
        assertEquals(0, passengers.size());
    }
}
