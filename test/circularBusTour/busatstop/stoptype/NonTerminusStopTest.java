package circularBusTour.busatstop.stoptype;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import circularBusTour.entities.Bus;
import circularBusTour.entities.BusStop;
import circularBusTour.entities.Passenger;

public class NonTerminusStopTest {

    private final NonTerminusStop stopType = new NonTerminusStop();
    private final Bus bus = new Bus(0);
    
    @Test
    public void getOffOnlyPassengersWhoWantToGetOffAtStop1() {
        bus.setCapacity(2);
        bus.getOn(new Passenger(1));
        bus.getOn(new Passenger(2));        
        BusStop stop = new BusStop(1);
        
        List<Passenger> passengers = stopType.getOff(bus, stop);
        assertEquals(1, passengers.size());
    }

    @Test
    public void busIsFullIfThereAreWaitingPassengersAtStop() {
        bus.setCapacity(1);
        BusStop stop = new BusStop(0);
        stop.pushPassenger(new Passenger(1));
        stop.pushPassenger(new Passenger(2));
        
        stopType.getOn(bus, stop);
        assertTrue(bus.isFull());
    }
}
