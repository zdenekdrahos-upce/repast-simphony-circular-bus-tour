package circularBusTour.analysis;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import circularBusTour.busatstop.BusAtStopStats;

public class BusAtStopAnalyzerTest {

    private final BusAtStopAnalyzer analyzer = new BusAtStopAnalyzer();
    
    @Before
    public void setUp() {
        analyzer.analyze(createStat(3, 0, 50));
        analyzer.analyze(createStat(7, 80, 150));
        analyzer.analyze(createStat(2, 100, 40));
    }
    
    private BusAtStopStats createStat(int delay, int occupancyRate, double waitingTime) {
        BusAtStopStats s = new BusAtStopStats();
        s.arrivalDelay = delay;
        s.busOccupancyRate = occupancyRate;
        s.waitingTimes.add(waitingTime);
        return s;
    }

    @Test
    public void assertDelays() {
        Stats s = analyzer.getDelays();
        assertEquals(2, s.getMin(), 0.0);
        assertEquals(4, s.getAvg(), 0.0);
        assertEquals(7, s.getMax(), 0.0);
    }

    @Test
    public void assertOccupancyRates() {
        Stats s = analyzer.getOccupancyRates();
        assertEquals(0, s.getMin(), 0.0);
        assertEquals(60, s.getAvg(), 0.0);
        assertEquals(100, s.getMax(), 0.0);
    }

    @Test
    public void assertWaitingTimes() {
        Stats s = analyzer.getWaitingTimes();
        assertEquals(40, s.getMin(), 0.0);
        assertEquals(80, s.getAvg(), 0.0);
        assertEquals(150, s.getMax(), 0.0);
    }

    @Test
    public void assertDelaysCount() {
        assertEquals(3, analyzer.getDelaysCount());
    }

    @Test
    public void assertNumberOfAnalyzedStats() {
        assertEquals(3, analyzer.getAnalyzedStatsCount());
    }

    @Test
    public void occupancyRateFromArrivalToTerminusIsNotCounted_becauseItIsAlwaysZero() {
        analyzer.reset();
        analyzer.analyze(createArrivalToTerminusStat());
        analyzer.analyze(createStat(80, 80, 80));
        
        Stats s = analyzer.getOccupancyRates();
        assertEquals(80, s.getMin(), 0.0);
        assertEquals(80, s.getAvg(), 0.0);
        assertEquals(80, s.getMax(), 0.0);
    }

    private BusAtStopStats createArrivalToTerminusStat() {
        BusAtStopStats s = new BusAtStopStats();
        s.idStop = 0;
        s.arrivalTime = 100;
        s.busOccupancyRate = 0;
        s.waitingTimes.clear();
        return s;
    }
}
