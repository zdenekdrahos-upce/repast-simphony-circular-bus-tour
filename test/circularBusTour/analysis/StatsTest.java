package circularBusTour.analysis;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class StatsTest {

    private final Stats stats = new Stats();

    @Before
    public void setUp() {
        stats.reset();
    }

    @Test
    public void newlyCreatedStatsReturnsZero() {
        assertEmptyStats();
    }

    @Test
    public void afterResetStatsReturnsZero() {
        stats.add(-5);
        stats.add(5);
        stats.reset();
        assertEmptyStats();
    }

    @Test
    public void ifOnlyZeroIsAddedThenAllStatsShouldBeZero() {
        stats.add(0);
        assertEmptyStats();
    }

    private void assertEmptyStats() {
        assertEquals(0, stats.getAvg(), 0.0);
        assertEquals(0, stats.getMin(), 0.0);
        assertEquals(0, stats.getMax(), 0.0);
    }

    @Test
    public void testAverage() {
        stats.add(1);
        stats.add(9);
        assertEquals(5, stats.getAvg(), 0.0);
    }

    @Test
    public void testMin() {
        stats.add(5);
        stats.add(2);
        assertEquals(2, stats.getMin(), 0.0);
    }

    @Test
    public void testMax() {
        stats.add(1);
        stats.add(0);
        assertEquals(1, stats.getMax(), 0.0);
    }
}
