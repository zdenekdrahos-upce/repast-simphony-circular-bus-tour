package circularBusTour.helpers;

import static org.junit.Assert.assertTrue;

import java.util.List;

import circularBusTour.busatstop.BusAtStopStats;
import circularBusTour.observer.StatsStorage;
import circularBusTour.CircularTour;
import circularBusTour.TourRequest;
import repast.simphony.engine.schedule.Schedule;

public class TourTestHelper {

    private Schedule schedule;
    private CircularTour tour;
    private TourRequest request;

    public TourTestHelper(Schedule schedule, CircularTour tour, TourRequest request) {
        this.schedule = schedule;
        this.tour = tour;
        this.request = request;
    }

    public void simulateCirculars(int circularsCount) {
        int lastBusAtTerminusCount = 0;
        int currentStop, previousStop = circularsCount;

        while (lastBusAtTerminusCount < circularsCount) {
            schedule.execute();
            currentStop = tour.getLastBusStop(request.bus.count - 1);
            if (currentStop != previousStop && currentStop == 0) {
                lastBusAtTerminusCount++;
            }
            previousStop = currentStop;
        }
    }

    public void assertThatNoTwoBusesAreAtSameStopAtSameTick() {
        StatsStorage storage = (StatsStorage) request.observers[0];
        List<BusAtStopStats> stats = storage.getStats();
        for (BusAtStopStats s : stats) {
            for (BusAtStopStats s2 : stats) {
                if (s != s2 && s.idStop == s2.idStop) {
                    assertTrue(s2.arrivalTime >= s.departureTime || s.arrivalTime >= s2.departureTime);
                }
            }
        }
    }
}
