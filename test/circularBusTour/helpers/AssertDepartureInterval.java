package circularBusTour.helpers;

import static org.junit.Assert.assertTrue;

import java.util.List;

import circularBusTour.busatstop.BusAtStopStats;

public class AssertDepartureInterval {

    private boolean isConstant = false;
    private int departureInterval;
    
    public void setInterval(int interval) {
        departureInterval = interval;
    }

    public void assertConstantInterval(List<BusAtStopStats> stats) {
        isConstant = true;
        assertDepartureIntervalFromTerminus(stats);
    }

    public void assertIntervalLargerOrEqualThanConstant(List<BusAtStopStats> stats) {
        isConstant = false;
        assertDepartureIntervalFromTerminus(stats);
    }

    private void assertDepartureIntervalFromTerminus(List<BusAtStopStats> stats) {
        double interval, current, previous = -1;
        for (BusAtStopStats s : stats) {
            if (s.idStop == 1) {
                if (previous == -1) {
                    previous = s.arrivalTime;
                    continue;
                }
                current = s.arrivalTime;
                interval = current - previous;
                assertInterval(interval);                
                previous = current;
            }
        }
    }

    private void assertInterval(double interval) {
        if (isConstant) {
            assertTrue(interval == departureInterval);
        } else {
            assertTrue(interval >= departureInterval);
        }
    }
    
}
