package circularBusTour.mocks;

import java.util.ArrayList;
import java.util.List;

import circularBusTour.busatstop.StopType;
import circularBusTour.entities.Bus;
import circularBusTour.entities.BusStop;
import circularBusTour.entities.Passenger;

public class NoStop implements StopType {

    private List<Passenger> noPassengers = new ArrayList<Passenger>();
    
    @Override
    public List<Passenger> getOff(Bus bus, BusStop stop) {
        return noPassengers;
    }

    @Override
    public List<Passenger> getOn(Bus bus, BusStop stop) {
        return noPassengers;
    }

}
