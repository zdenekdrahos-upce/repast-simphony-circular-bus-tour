package circularBusTour.mocks;

import repast.simphony.engine.schedule.Schedule;

public class TickSchedule extends Schedule {

    private double tickCount;

    public TickSchedule(double tickCount) {
        this.tickCount = tickCount;
    }
    
    @Override
    public double getTickCount() {
        return tickCount;
    }
    
}
