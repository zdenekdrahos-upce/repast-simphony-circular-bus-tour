package circularBusTour.mocks;

import circularBusTour.busatstop.BusAtStopStats;
import circularBusTour.observer.BusAtStopObserver;

public class StopStorageObserver implements BusAtStopObserver {

    public BusAtStopStats lastStopsStats;

    @Override
    public void update(BusAtStopStats lastStopStats) {
        this.lastStopsStats = lastStopStats;
    }

}
