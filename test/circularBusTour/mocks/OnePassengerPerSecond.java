package circularBusTour.mocks;

import circularBusTour.generator.DurationGenerator;

public class OnePassengerPerSecond implements DurationGenerator {

    @Override
    public double next() {
        return 1;
    }

}
