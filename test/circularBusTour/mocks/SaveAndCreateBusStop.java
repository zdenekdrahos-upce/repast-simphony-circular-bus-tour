package circularBusTour.mocks;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import circularBusTour.entities.BusStop;
import circularBusTour.entities.BusStopFactory;
import circularBusTour.entities.Passenger;

public class SaveAndCreateBusStop implements BusStopFactory {

    public final List<Integer> usedIds = new ArrayList<>();
    public final Map<Integer, Integer> numberOfPassengers = new HashMap<>();
    
    @Override
    public BusStop create(int id) {
        usedIds.add(id);
        numberOfPassengers.put(id, 0);
        return new TestBusStop(id);
    }

    private class TestBusStop extends BusStop {

        public TestBusStop(int id) {
            super(id);
        }
        
        @Override
        public void pushPassenger(Passenger passenger) {
            super.pushPassenger(passenger);
            incrementPassengersCount();
        }

        private void incrementPassengersCount() {
            int idStop = super.getId();
            int current = numberOfPassengers.get(idStop);
            numberOfPassengers.put(idStop, current + 1);
        }
    }
}
