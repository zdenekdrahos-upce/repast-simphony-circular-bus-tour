package circularBusTour;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import circularBusTour.busatstop.BusAtStopStats;
import circularBusTour.entities.Bus;
import circularBusTour.generator.exponential.ExponentialGenerator;
import circularBusTour.helpers.AssertDepartureInterval;
import circularBusTour.helpers.TourTestHelper;
import circularBusTour.mocks.SaveAndCreateBusStop;
import circularBusTour.observer.BusAtStopObserver;
import circularBusTour.observer.StatsStorage;
import circularBusTour.repast.Scheduler;
import repast.simphony.context.DefaultContext;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.environment.RunState;
import repast.simphony.engine.schedule.Schedule;

public class CircularTourTest {
      
    private Schedule schedule;
    private CircularTour tour;
    private final TourRequest request = new TourRequest();
    private TourTestHelper tourTester;
    private final AssertDepartureInterval departureInterval = new AssertDepartureInterval();
    private final StatsStorage stats = new StatsStorage();
    
    @Before
    public void setUp() {
        schedule = new Schedule();
        RunEnvironment.init(schedule, null, null, true);
        RunState.init().setMasterContext(new DefaultContext<Object>());
        OneTimeScheduler scheduler = new Scheduler(schedule);
        tour = new CircularTour(scheduler);
        tourTester = new TourTestHelper(schedule, tour, request);
        request.observers = new BusAtStopObserver[] {stats};
    }

    @Test
    public void busShouldWaitAtTerminusBeforeSimulation() {        
        request.bus.count = 1;
        request.stop.count = 2;
        tour.execute(request);
        assertEquals(0, tour.getLastBusStop(0));
    }

    @Test
    public void busShouldWaitAtTerminusIfArrivesBackFromTour() {
        request.interval.terminusDeparture = 1000;
        request.interval.timeBetweenStops = 100;
        request.bus.count = 1;
        request.stop.count = 2;
        tour.execute(request);                
        simulateThreeCirculars();

        int expectedTimes[] = {0, 100, 200, 1100, 1200, 2100, 2200};        
        assertBusArrivals(0, expectedTimes);
    }

    @Test
    public void whenBusMissesDepartureTerminusThenBusShouldGoBackOnRoadWithoutStop() {
        request.interval.terminusDeparture = 1000;
        request.interval.timeBetweenStops = 600;
        request.bus.count = 1;
        request.stop.count = 2;
        tour.execute(request);                
        simulateThreeCirculars();

        int expectedTimes[] = {0, 600, 1200, 1800, 2400, 3000, 3600};
        assertBusArrivals(0, expectedTimes);
    }

    @Test
    public void testOneCircularForTourWithTwoBusesAndThreeStops() {                
        request.interval.terminusDeparture = 120;
        request.interval.timeBetweenStops = 120;
        request.bus.count = 2;
        request.stop.count = 3;
        tour.execute(request);

        // arrival to stop #1, #2 and back to terminus
        int expectedTimesA[] = {0, 120, 240, 360};
        int expectedTimesB[] = {0, 240, 360, 480};

        assertBusArrivals(0, expectedTimesA);
        assertBusArrivals(1, expectedTimesB);
    }

    private void assertBusArrivals(int idBus, int[] expectedTimes) {
        int currentIndex = 0, currentStop = 0;
        for (BusAtStopStats s : stats.getStats()) {
            if (s.idBus == idBus) {
                assertEquals(currentStop, s.idStop);
                assertEquals(expectedTimes[currentIndex], s.arrivalTime, 0.0);
                currentIndex++;
                if (currentStop == request.stop.count) {
                    currentStop = 0;
                }
            }
            if (expectedTimes.length >= currentIndex) {
                return;
            }
        }
    }

    @Test
    public void passengersAreComingToAllBusStops() {
        SaveAndCreateBusStop factory = new SaveAndCreateBusStop();
        request.interval.terminusDeparture = 1200;
        request.interval.timeBetweenStops = 120;
        request.bus.count = 1;
        request.bus.capacity = 1;
        request.stop.count = 8;
        request.stop.incomingPassengers = new ExponentialGenerator(100);
        request.stop.factory = factory;
        tour.execute(request);

        simulateThreeCirculars();        
        assertNoBusStopIsEmpty(factory);
    }
    
    private void simulateThreeCirculars() {
        tourTester.simulateCirculars(3);
    }

    private void assertNoBusStopIsEmpty(SaveAndCreateBusStop factory) {
        for (int i = 0; i < request.stop.count; i++) {
            assertTrue(factory.numberOfPassengers.get(i) > 0);
        }
    }

    @Test(expected=Bus.FullCapacity.class)
    public void whenBusCapacityIsZeroPassengerCannotGetOnTheBus_exceptionShouldBeThrown() {
        request.interval.terminusDeparture = 1200;
        request.interval.timeBetweenStops = 100;
        request.bus.count = 1;
        request.bus.capacity = 0;
        request.stop.count = 8;
        request.stop.incomingPassengers = new ExponentialGenerator(100);
        tour.execute(request);        
        simulateThreeCirculars();
    }

    @Test
    public void testPassengersWaitsForNextNonFullBusIfBusIsFullAndDepartureIntervalIsNotCutted() {
        request.interval.terminusDeparture = 200;
        request.interval.timeBetweenStops = 100;
        request.bus.count = 30;
        request.bus.capacity = 20;
        request.stop.count = 8;
        request.stop.incomingPassengers = new ExponentialGenerator(10);
        request.busAtStop.on = new ExponentialGenerator(3);
        request.busAtStop.off = new ExponentialGenerator(3);
        tour.execute(request);        
        
        simulateThreeCirculars();            
        assertThatEveryPassengersWaitsAtLeastOneTick();
        departureInterval.setInterval(request.interval.terminusDeparture);
        departureInterval.assertIntervalLargerOrEqualThanConstant(stats.getStats());
    }

    private void assertThatEveryPassengersWaitsAtLeastOneTick() {
        for (BusAtStopStats s : stats.getStats()) {
            for (double waitingTime : s.waitingTimes) {
                assertTrue(waitingTime > 0);
            }
        }
    }

    @Test
    public void departureIntervalIsConstantIfNoPassengersGetOnAndOff() {
        request.interval.terminusDeparture = 200;
        request.interval.timeBetweenStops = 100;
        request.bus.count = 30;
        request.bus.capacity = 20;
        request.stop.count = 8;
        tour.execute(request);        
        
        simulateThreeCirculars();
        departureInterval.setInterval(request.interval.terminusDeparture);
        departureInterval.assertConstantInterval(stats.getStats());
    }

    @Test
    public void testBusesAreDelayedIfTwoBusesAreAtSameStopAtSameTick() {
        request.interval.terminusDeparture = 500;
        request.interval.timeBetweenStops = 120;
        request.bus.count = 8;
        request.bus.capacity = 120;
        request.stop.count = 20;
        request.stop.incomingPassengers = new ExponentialGenerator(5);
        request.busAtStop.on = new ExponentialGenerator(5);
        request.busAtStop.off = new ExponentialGenerator(5);
        tour.execute(request);        
        
        simulateThreeCirculars();
        assertTrue(existsDelayedBus());
        tourTester.assertThatNoTwoBusesAreAtSameStopAtSameTick();      
    }

    private boolean existsDelayedBus() {
        for (BusAtStopStats s : stats.getStats()) {
            if (s.arrivalDelay > 0) {
                return true;
            }
        }
        return false;
    }
}
