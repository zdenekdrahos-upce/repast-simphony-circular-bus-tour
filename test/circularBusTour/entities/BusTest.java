package circularBusTour.entities;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class BusTest {

    private Bus bus = new Bus(1);

    @Test
    public void getIdReturnsIntegerPassedInConstructor() {
        assertEquals(1, bus.getId());
    }

    @Test
    public void newlyCreatedBusIsEmpty() {
        assertFalse(bus.isFull());
    }

    @Test
    public void testBusFullOfPassengers() {
        bus.setCapacity(1);
        addPassenger();
        assertTrue(bus.isFull());
    }

    @Test(expected=Bus.FullCapacity.class)
    public void testBusCapacityOverflow() {
        bus.setCapacity(1);
        addPassenger();
        addPassenger();
    }

    @Test(expected=Bus.FullCapacity.class)
    public void newlyCreatedBusHasZeroCapacity_overflowShouldBeThrown() {
        addPassenger();
    }

    private void addPassenger() {
        bus.getOn(new Passenger(0));
    }

    @Test
    public void newlyCreatedBusHasZeroOccupancyRate() {
        assertEquals(0, bus.getOccupancyRate(), 0.0);
    }

    @Test
    public void fullBusHasOccupancyRate100() {
        bus.setCapacity(1);
        bus.getOn(new Passenger(0));
        assertEquals(100, bus.getOccupancyRate(), 0.0);
    }

    @Test
    public void testBusWith25percentOccupancyRate() {
        bus.setCapacity(4);
        bus.getOn(new Passenger(0));
        assertEquals(25, bus.getOccupancyRate(), 0.0);
    }
}
