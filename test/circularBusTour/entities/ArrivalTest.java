package circularBusTour.entities;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class ArrivalTest {

    private final Arrival arrival = new Arrival();    

    @Test
    public void newlyCreatedArrivalIsNotDelayed() {
        assertNotDelayedArrival();
    }

    private void assertNotDelayedArrival() {
        assertEquals(0, arrival.getDelay(), 0.0);
    }

    private void assertDelayedArrival(int delay) {
        double arrivalDelay = arrival.getDelay();
        assertTrue(arrivalDelay > 0);
        assertEquals(delay, arrivalDelay, 0.0);
    }

    @Test
    public void testPreceding() {
        arrival.increaseTime(10);
        assertTrue(arrival.isPreceding(20));
        assertFalse(arrival.isPreceding(10));
        assertFalse(arrival.isPreceding(5));
    }

    @Test
    public void arrivalIsDelayedIfDepartureIsLargerThanCurrentTime() {
        arrival.increaseTime(0);
        arrival.delay(10);
        assertDelayedArrival(10);
    }

    @Test
    public void arrivalIsNotDelayedIfDepartureIsSmallerThanCurrentTime() {
        arrival.increaseTime(10);
        arrival.delay(5);
        assertNotDelayedArrival();
    }

    @Test
    public void arrivalIsNotDelayedIfDepartureEqualsCurrentTime() {
        arrival.increaseTime(0);
        arrival.delay(0);
        assertNotDelayedArrival();
    }

    @Test
    public void delayIsIncludedInTime() {
        arrival.increaseTime(10);
        arrival.delay(20);
        assertEquals(20, arrival.getTime(), 0.0);
        assertEquals(10, arrival.getDelay(), 0.0);
    }

    @Test
    public void testTimeForNonDelayedArrival() {
        arrival.increaseTime(10);
        assertEquals(10, arrival.getTime(), 0.0);
    }

    @Test
    public void increaseShouldBeAddedToExistingTime() {
        arrival.increaseTime(10);
        arrival.increaseTime(40);
        assertEquals(50, arrival.getTime(), 0.0);
    }

    @Test
    public void setTimeReplaceAllPreviousTicks() {
        arrival.increaseTime(10);
        arrival.increaseTime(40);        
        arrival.setTime(200);
        assertEquals(200, arrival.getTime(), 0.0);
    }

    @Test
    public void setTimeDontChangeDelay() {
        arrival.increaseTime(10);
        arrival.delay(20);        
        arrival.setTime(100);
        assertEquals(110, arrival.getTime(), 0.0);
    }

    @Test
    public void doubleDelay() {
        arrival.increaseTime(10);
        arrival.delay(20);
        assertEquals(20, arrival.getTime(), 0.0);
        assertEquals(10, arrival.getDelay(), 0.0);
        
        arrival.delay(30);
        assertEquals(30, arrival.getTime(), 0.0);
        assertEquals(20, arrival.getDelay(), 0.0);
    }

    @Test
    public void arrivalIsNotDelayedIfDelayIsAddedToTick() {
        arrival.increaseTime(10);
        arrival.delay(20);
        arrival.addDelayToTime();
        assertEquals(20, arrival.getTime(), 0.0);
        assertEquals(0, arrival.getDelay(), 0.0);
    }

    @Test
    public void addDelayButDontChangeDepartureTime() {
        arrival.increaseTime(100);
        arrival.addPreviousDelay(20);
        assertEquals(100, arrival.getTime(), 0.0);
        assertEquals(20, arrival.getDelay(), 0.0);
    }
}
