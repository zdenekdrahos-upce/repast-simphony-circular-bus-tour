package circularBusTour.entities;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class BusStopTest {

    private BusStop busStop = new BusStop(1);

    @Test
    public void getIdReturnsIntegerPassedInConstructor() {
        assertEquals(1, busStop.getId());
    }

    @Test
    public void newlyCreatedStopIsEmpty() {
        assertFalse(busStop.areThereWaitingPassengers());
    }

    @Test
    public void afterPassengerIsPushed_StopIsNotEmpty() {        
        busStop.pushPassenger(new Passenger(0));
        assertTrue(busStop.areThereWaitingPassengers());
    }

    @Test
    public void passengersPushPopShouldBeFirstInFirstOut() {        
        Passenger first = new Passenger(0);
        Passenger second = new Passenger(0);
        
        busStop.pushPassenger(first);
        busStop.pushPassenger(second);
        
        assertEquals(first, busStop.popPassenger());
        assertEquals(second, busStop.popPassenger());
    }

    @Test(expected=BusStop.NoPassengersAtStop.class)
    public void whenEmptyStopIsPoppedExceptionIsThrown() {        
        busStop.popPassenger();
    }
}
