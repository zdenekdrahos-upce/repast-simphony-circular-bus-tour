package circularBusTour.entities;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class PassengerTest {
    
    @Test
    public void waitingTimeIsBusArrivalTimeMinusIncomingTime() {
        Passenger p = new Passenger(0);
        p.setIncomingTime(1);
        p.setBusArrivalTime(10);
        assertEquals(9, (int)p.getWaitingTime());
    }

    @Test
    public void testChangeToNextBusAtTerminus() {
        Passenger p = new Passenger(0);
        p.setIncomingTime(1);
        p.setBusArrivalTime(10);
        
        p.changeToNextBus(25);
        p.setBusArrivalTime(36);
        assertEquals(20, (int)p.getWaitingTime());
    }
}
