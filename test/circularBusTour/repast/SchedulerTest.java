package circularBusTour.repast;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import repast.simphony.context.DefaultContext;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.environment.RunState;
import repast.simphony.engine.schedule.IAction;
import repast.simphony.engine.schedule.ISchedule;
import repast.simphony.engine.schedule.Schedule;
import circularBusTour.mocks.NullAction;

public class SchedulerTest {


    @Test
    public void onlyOneNonRepeatingArrivalIsScheduled() {
        int tick = 20;
        
        IAction action = new NullAction();
        ISchedule schedule = new Schedule();
        Scheduler scheduler = createRepastSchedule(schedule);
        
        scheduler.schedule(tick, action);        

        assertEquals(1, schedule.getActionCount());        
        schedule.execute();              
        assertEquals(0, schedule.getActionCount());
    }

    private Scheduler createRepastSchedule(ISchedule schedule) {
        RunEnvironment.init(schedule, null, null, true);
        RunState.init().setMasterContext(new DefaultContext<Object>());
        return new Scheduler(schedule);
    }
}
