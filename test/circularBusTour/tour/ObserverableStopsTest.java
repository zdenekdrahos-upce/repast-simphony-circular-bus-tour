package circularBusTour.tour;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import circularBusTour.busatstop.BusAtStopStats;
import circularBusTour.mocks.StopStorageObserver;


public class ObserverableStopsTest {

    private final ObservableStops observable = new ObservableStops();
    private final BusAtStopStats stats = new BusAtStopStats();
    
    @Test
    public void noErrorIfNoObservers() {
        observable.register(null);
        observable.notify(stats);
    }

    @Test
    public void allRegisteredObserverShouldBeNotified() {
        StopStorageObserver observers[] = createThreeObservers();
        observable.register(observers);               
        observable.notify(stats);
        
        for (StopStorageObserver o : observers) {
            assertEquals(stats, o.lastStopsStats);
        }
    }

    private StopStorageObserver[] createThreeObservers() {
        return new StopStorageObserver[] {
                new StopStorageObserver(),
                new StopStorageObserver(),
                new StopStorageObserver()
        };
    }

}
