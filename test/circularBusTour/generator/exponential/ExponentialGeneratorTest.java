package circularBusTour.generator.exponential;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class ExponentialGeneratorTest {

    private ExponentialGenerator generator;

    @Test
    public void meanOfIntervalsBetweenTicksShouldBeSimilarToInputMean() {
        double mean = 5;
        double ticksCount = 100;
        double tolerance = 2;

        generator = new ExponentialGenerator(mean);  
        double calculatedMean = sumIntervalsBetweenTicks(ticksCount) / ticksCount;
        double difference = Math.abs(mean - calculatedMean);

        assertTrue(difference < tolerance);
    }

    private double sumIntervalsBetweenTicks(double count) {
        double sum = 0;
        for (int i = 0; i < count; i++) {
            sum += generator.next();
        }
        return sum;
    }

}
